# README #

This is new design version of the web application that helps students Worldwide to find a room for rent in the cities where the University of Cadiz is. It is ready to deploy though it is at alpha stage, so some bugs probably will occur.

It has been design with three focus:

* Several languages: it will be used by students who still don't know Spanish
* Very easy to use: landlords are usually elderly people
* Following the Grails standards: it will be maintained by others developers in the future.


## Feautures ##

* Roles of user, admin and superadmin: the superior roles can create, modify and delete the inferior ones. If there is not any superadmin, grails will create a new one with username superadmin and password superadmin (remember to change the password!)
* Uploaded images will be compressed and a watermark will be added to avoid to be copied to other websites.
* Late registration: users can see the available rooms for rent. They will be asked to register if they want to see details
* Landlords must fill all the features of the the apartment / room like: ¿Microwave? Yes/No. ¿TV? Yes/No
* Google map of each room. Coordinates are stored for future use (like putting in the same map all the apartments if you wish to implement)


## Technologies ##

It has been build with the default technologies of Grails. It works in any Java server. It uses Twitter Bootstrap in the frontend. It uses the H2 database though it can be changed to mysql easily. See below for installation instructions.


## TO DO ##

* Responsive version for mobile phones. It has been build with Twitter Bootstrap, so it should not be a hard task.
* Rest of stuff to do in the issues tracking: https://bitbucket.org/chelder86/webalojamientouca/issues?status=new&status=open

## Setup ##

### Installation ###

* Install Grails 2.2.4. Maybe you can use a newer version of Grails. Grails should update everything automatically
* I was using Spring STS as my IDE. You can use eclipse if you wish though.
* It uses the database H2. It is integrated in Grails, so you don't need to download anything. You can change it to mysql or some others in the DataSource.groovy fairly easy
* Apache Tomcat is included. I was using a separated instance of it though.
* Plugins and dependencies are downloaded automatically from the Maven and the Grails repository.


### Deployment ###

Having grails in the PATH, just execute within the working directory:

* grails test war: to run test, then deploy
* grails dev war: to create a war for the development environment
* More information here: http://grails.github.io/grails-doc/3.0.x/ref/Command%20Line/war.html

I have uploaded and tested it succesfully in appfog: https://www.appfog.com