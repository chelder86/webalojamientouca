//	El código de este js debe estar en el head.

var geocoder;
var map;
var mapOptions;
var marker;


function initialize() {

  var lat = document.getElementById('latitud_norte').innerHTML;  
  var lng = document.getElementById('longitud_oeste').innerHTML;
  
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(lat, lng);
  mapOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP    
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
    marker = new google.maps.Marker({
      position: latlng,
      map: map      
  });  
  
  marker.setVisible(true);   
}


google.maps.event.addDomListener(window, 'load', initialize);

