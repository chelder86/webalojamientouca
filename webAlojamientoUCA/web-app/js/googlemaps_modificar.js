
var geocoder;
var map;
var mapOptions;
var marker;


function checkIfEnterPushed (e, nombreDeBotonAHacerClick)
{
    if (null == e)
        e = window.event ; //< Truco para que vaya en Internet Explorer también.
    if (e.keyCode == 13)  {
        document.getElementById(nombreDeBotonAHacerClick).click();
        return false;
    }
}


function desactivarCoordenadasManual() {
	$('#latitud_norte').prop('readonly','true');
	$('#longitud_oeste').prop('readonly','true');	
	$('#botonCoordenadas').prop('disabled', true); // Disables visually + functionally
	$("#botonCoordenadas").removeClass( "btn-danger" );
}

function activarCoordenadasManual() {	
	$('#latitud_norte').removeProp('readonly');
	$('#longitud_oeste').removeProp('readonly');
	$('#botonCoordenadas').removeProp('disabled');
	$("#botonCoordenadas").addClass( "btn-danger" );	
}


function checkbuttonCoordenadasManualClick() {	
    if (document.getElementById('establecerCoordenadasManualmente').checked) {
        activarCoordenadasManual();
    } else {
        desactivarCoordenadasManual();
    }
}


function initializeCreate() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(26.893231, 112.571997);
  mapOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN    
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
    marker = new google.maps.Marker({
      position: latlng,
      map: map      
  });  
  
  marker.setVisible(false);
  
  desactivarCoordenadasManual();  
}


function initializeEdit() {

  var lat = document.getElementById('latitud_norte').value;
  var lng = document.getElementById('longitud_oeste').value;
  
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(lat, lng);
  mapOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP    
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
    marker = new google.maps.Marker({
      position: latlng,
      map: map      
  });  
  
  marker.setVisible(true);   
}


function dibujarCoordenadasEnMapa() {
  
  var lat = document.getElementById('latitud_norte').value;
  var lng = document.getElementById('longitud_oeste').value;
  var latlng = new google.maps.LatLng(lat, lng);   
  
  mapOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP    
  } ; 
  
  
  map.setCenter(latlng);   
  map.setOptions(mapOptions);
         
  marker.setPosition(latlng); //< Truco para que no dibuje varios markers 
  marker.setVisible(true);
}


function codeAddress() {  
  
  var errorObtencionCoordenadas = messages.alojamiento.gmap.errorobtencioncoordenadas(); 


  var el = document.getElementById("localidad");
  var localidad = el.options[el.selectedIndex].text;
  
  // Cambia los casos Cádiz: centro o Cádiz: parte nueva por simplemente Cádiz:
  if ((localidad.indexOf("Cádiz") != -1) || (localidad.indexOf("Cadiz") != -1)){
  	localidad = "Cadiz";
  }
    
  var direccion = document.getElementById('direccion').value;

 var address = direccion + ', ' + localidad;  
  
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    	
      document.getElementById('latitud_norte').value = results[0].geometry.location.lat();
      document.getElementById('longitud_oeste').value = results[0].geometry.location.lng(); 
    
      map.setCenter(results[0].geometry.location);            
      mapOptions = {
		zoom: 15,
		center: results[0].geometry.location,
		mapTypeId: google.maps.MapTypeId.ROADMAP    
	  } ; 
                  
      marker.setPosition(results[0].geometry.location);
      marker.setVisible(true);
      
      map.setOptions(mapOptions);

    } else {
      alert(errorObtencionCoordenadas + ' ' + status);
    }
  });
}

google.maps.event.addDomListener(window, 'load', checkbuttonCoordenadasManualClick);

// Según se trate de edit o de create inicializa Google Maps de una manera u otra:
if (window.location.pathname.split('/')[3] == "create"){
	google.maps.event.addDomListener(window, 'load', initializeCreate);
}
else {
	google.maps.event.addDomListener(window, 'load', initializeEdit);
} 
