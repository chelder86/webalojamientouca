package usuario

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils 
import org.springframework.context.MessageSource;
 

class UsuarioService {

	def springSecurityService
	def MessageSource messageSource
	def uploadImageService
	
	/**
	 * Borra el rol actual y añade el nuevo.
	 *  > Rollback si ocurre cualquier excepción (incluidas las no Runtime)
	 * @param usuarioInstance
	 * @param rolInstance
	 * @return
	 */	
	@org.springframework.transaction.annotation.Transactional(rollbackFor=Exception.class) 
	def updateRol (usuarioInstance, rolInstance) {
		UsuarioRol.removeAll usuarioInstance
		//throw new RuntimeException("error entre uno y otro") // <- Para probar si hace rollback correctamente.
		UsuarioRol.create usuarioInstance, rolInstance
	}
	
	/**
	 *  TODO Borra al usuario actual, eliminando previamente sus dependencias.
	 *  > Rollback si ocurre cualquier excepción (incluidas las no Runtime)
	 * @param usuarioInstance
	 * @param rolInstance
	 * @return
	 */
	@org.springframework.transaction.annotation.Transactional(rollbackFor=Exception.class)
	def deleteUser (usuarioInstance) {
				

			
			// Y los alojamientos que ha publicado el usuario:
			Collection<alojamiento.Alojamiento> alojamientos = alojamiento.Alojamiento.findAllByUsuario(usuarioInstance);
			alojamientos*.delete();
			
			// Borramos las imágenes asociadas al alojamiento:			
			alojamientos.each() { 
				uploadImageService.deleteImageAndThumbnailDirectories(it.id)
			}; 			
			
			// Borramos las entradas correspondientes de la tabla intermedia de UsuarioRol:
			Collection<UsuarioRol> usuarioRoles = UsuarioRol.findAllByUsuario(usuarioInstance);
			usuarioRoles*.delete();	
						
			// Ahora ya podemos proceder al borrado del usuario:
			usuarioInstance.delete(flush: true)

		
	}
	
	
	/**
	 * Los usuarios no pueden ver los datos de otros usuarios:
	 * @param id del Usuario. Si id = -1 no se permite la visualización al usuario (con rol usuario).
	 * @return
	 */
	def comprobarPermisosDeVisualizacion(id) {
		
		def titleText = messageSource.getMessage("usuario.service.visualizacion.titletext.nopermitido", null, null, null)
		def descriptionText = messageSource.getMessage("usuario.service.visualizacion.descriptiontext.nopermitido", null, null, null)
		def visualizacionPermitida = true
				
		if (SpringSecurityUtils.ifAllGranted('ROL_USUARIO')) {
			if (id == -1
			 || Usuario.get(id).username != springSecurityService.authentication.name) {
				visualizacionPermitida = false
			}
		}
		
		[visualizacionPermitida: visualizacionPermitida, titleText: titleText, descriptionText: descriptionText]
	}
	
	
	
	
	
	/**
	 *  Un usuario con un rol únicamente puede modificar usuarios con rol inferior (o a sí mismo):
	 * @param id del Usuario
	 * @return
	 */
	def comprobarPermisosDeModificacion(id) {
		
		def titleText = messageSource.getMessage("usuario.service.modificacion.titletext.nopermitido", null, null, null)
		def descriptionText = messageSource.getMessage("usuario.service.modificacion.descriptiontext.nopermitido", null, null, null)
		def modificacionPermitida = true
		
		// Si tiene rol usuario únicamente puede editarse a sí mismo:
		if (SpringSecurityUtils.ifAllGranted('ROL_USUARIO')) {
			if (Usuario.get(id).username != springSecurityService.authentication.name) {
				modificacionPermitida = false
			}
		}

		// Si tiene rol admin puede editarse a sí mismo u otros con rol usuario:
		else if (SpringSecurityUtils.ifAllGranted('ROL_ADMIN')){
			if (Usuario.get(id).username != springSecurityService.authentication.name
			&& !Usuario.get(id).authorities.any { it.authority == "ROL_USUARIO" }) {
				modificacionPermitida = false 
			}
		}

		// Si es superadmin puede editarse a sí mismo u otros que no sean superadmin:
		else if (SpringSecurityUtils.ifAllGranted('ROL_SUPERADMIN')){
			if (Usuario.get(id).username != springSecurityService.authentication.name
			&& Usuario.get(id).authorities.any { it.authority == "ROL_SUPERADMIN" }) {
				modificacionPermitida = false
			}
		}
		
		[modificacionPermitida: modificacionPermitida, titleText: titleText, descriptionText: descriptionText]
	}
	
	
	
	
	/**
	 * TODO cuando se produce un cambio de rol, no se hace efectivo hasta un logout+login
	 * Hace que el cambio de rol de un usuario se haga efectivo inmediatamente
	 * @return
	 */	 
	def recargarSistemaConNuevoRol() {		
		
		// Quizás la solución de aquí funcione:
		// http://stackoverflow.com/questions/9910252/how-to-reload-authorities-on-user-update-with-spring-security
		
		// Esto no hace nada:
		//SecurityContextHolder.getContext().setAuthentication(SecurityContextHolder.getContext().getAuthentication());
	}
	
}

