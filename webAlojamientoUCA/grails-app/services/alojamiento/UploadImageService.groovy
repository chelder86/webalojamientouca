package alojamiento

import java.awt.image.BufferedImage
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import javax.imageio.ImageIO
import org.springframework.context.MessageSource;


class UploadImageService {
	
	def MessageSource messageSource

	/**
	 *  Las imágenes se pueden alojar en distintos lugares.
	 *  Esta variable siempre tendrá la ruta donde se están guardando actualmente
	 */	 
	def currentImagesPath = getExternalImagesPath()
	//def imagesPath = "/tmp"
	
	/**
	 *  Esta variables guardan el formato en que se guardarán las imágenes.
	 *   > Se puede optar por jpg, png y gif:
	 */	 
	def thumbnailsExtension = "jpg"
	def imagesExtension = "jpg"
	
	
	/**
	 * 
	 * @param fileName
	 * @return String
	 */
	def removeFileExtension (String fileName) {
		if(fileName.contains(".")) 
			return fileName.substring(0, fileName.lastIndexOf('.'))
	}
	
	/**
	 * 
	 * @param alojamientoId
	 * @return String
	 */
	def getImageDirectoryDestinationPath (alojamientoId) {
		return currentImagesPath + "\\" + alojamientoId;
	}
	
	
	/**
	 * Devuelve la ruta completa donde se almacenará la imagen, 
	 * cambiandole la extensión a .jpg
	 * @param alojamientoId
	 * @param imageName
	 * @return String
	 */	 
	def getImageDestinationPath (alojamientoId, String imageName) {
		return getImageDirectoryDestinationPath(alojamientoId) + "\\" + imageName;
	}
	
	/**
	 *  Devuelve la ruta completa donde se almacenará la miniatura,
	 *  cambiandole la extensión a .jpg
	 * @param alojamientoId
	 * @param imageName
	 * @return String
	 */
	def getThumbnailDestinationPath (alojamientoId, String imageName) {	
		return getThumbnailDirectoryDestinationPath(alojamientoId) + "\\" + imageName;
	}
	
	
	/**
	 * 
	 * @param alojamientoId
	 * @return String
	 */
	def getThumbnailDirectoryDestinationPath (alojamientoId) {
		return currentImagesPath + "\\" + alojamientoId + "_thumbnail";
	}
	

	
	/**
	 *  Devuelve la ruta que a continuación se describe, fuera del context path.
	 *
	 * 	La ruta sería:
	 *		- directorio donde se encuentra el directorio con la aplicación web
	 *			- webAlojamiento
	 *				- webapp
	 *				- src
	 *				- ...
	 *			- externalImages <<<<<<
	 *
	 * @return String
	 */
	private getExternalImagesPath () {
		// En el controller era algo más sencillo: String realWebAppPath = servletContext.getRealPath("/");
		String realWebAppPath = org.codehaus.groovy.grails.web.context.ServletContextHolder.getServletContext().getRealPath("/");
		String parentOfWebAppPath = new File(realWebAppPath).getParentFile().getParent();
		return parentOfWebAppPath + "\\externalImages"		
	}
	
	/**
	 *  Crea una ruta si ya no existía previamente:
	 * @param path
	 * @return ¿File? ¿null?
	 */
	def createDirectoryIfNotExist(String path) {
		def path2 = new File(path);
		//if( !path2.exists() ) {		 
		  path2.mkdirs()
		//}	
	}
	
	/**
	 *  Guarda la imagen con unas dimensiones máximas y añadiéndole una marca de agua:
	 * @param originalImage
	 * @param alojamientoId
	 * @param imageName
	 * @return
	 */
	def saveImage(BufferedImage originalImage, alojamientoId, String imageName) {
				
		if (alojamientoId == null || alojamientoId == "null") {
			return			
		}
		
		createDirectoryIfNotExist(getImageDirectoryDestinationPath(alojamientoId));
		def watermarkPath = org.codehaus.groovy.grails.commons.ApplicationHolder.application.parentContext.getResource("images/logo_oficina_alojamiento.png").file.getAbsolutePath()
		def watermarkImage = ImageIO.read(new File(watermarkPath))

		// Si excede anchura o altura permitidas la redimensiona:
		if (originalImage.getWidth() > 1024 || originalImage.getHeight() > 768) {			
			Thumbnails.of(originalImage)
				.size(1024, 768)				
				.outputQuality(0.8f)
				.outputFormat(imagesExtension)
				.watermark(Positions.CENTER, watermarkImage, 0.1f)
				.toFile(new File(getImageDestinationPath(alojamientoId, imageName)));			
		}
		
		// Si ambas son menores que la permitida guarda la imagen con su tamaño original:	
		else {
			Thumbnails.of(originalImage)
				.scale(1f)
				.outputQuality(0.8f)
				.outputFormat(imagesExtension)
				.watermark(Positions.CENTER, watermarkImage, 0.07f)
				.toFile(new File(getImageDestinationPath(alojamientoId, imageName)));
		}
	}
		
		
		

	
	/**
	 *  Guarda una miniatura de la imagen original
	 * @param originalImage
	 * @param alojamientoId
	 * @param imageName
	 * @return
	 */
	def saveThumbnail(BufferedImage originalImage, alojamientoId, String imageName) {
		
		if (alojamientoId == null || alojamientoId == "null") {			
			return			
		}
		
		def watermarkPath = org.codehaus.groovy.grails.commons.ApplicationHolder.application.parentContext.getResource("images/logo_oficina_alojamiento_small.png").file.getAbsolutePath()
		def watermarkImage = ImageIO.read(new File(watermarkPath))
		
		createDirectoryIfNotExist(getThumbnailDirectoryDestinationPath(alojamientoId));
		Thumbnails.of(originalImage)
			.size(900, 250)			
			.outputQuality(0.9f)
			.watermark(Positions.BOTTOM_RIGHT, watermarkImage, 0.2f)
			.outputFormat(thumbnailsExtension)
			.toFile(new File(getThumbnailDestinationPath(alojamientoId, imageName)));
	}
	
	
	/**
	 * 
	 * @param alojamientoId
	 * @return
	 */
	def deleteImageAndThumbnailDirectories(alojamientoId) {		
		File imageDirectory = new File(getThumbnailDirectoryDestinationPath(alojamientoId));
		File thumbnailDirectory = new File(getImageDirectoryDestinationPath(alojamientoId));
		imageDirectory.deleteDir()
		thumbnailDirectory.deleteDir()		
	}
	
	
	
	/**
	 * Devuelve la ruta a las imágenes, miniaturas y sus nombres de un alojamiento
	 * @param idAlojamiento
	 * @return Map de Maps. Ir a _imagesUploadShow.gsp para ver como recorrer los datos.
	 */
	def getImagesForCarousel(idAlojamiento) {
		
		def results = []
		String baseName;
		String imagesDirectoryPath = getImageDirectoryDestinationPath(idAlojamiento);
		def dir = new File(imagesDirectoryPath)
		
		def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()
		
		if (dir.exists()) {
			dir.eachFile { // Toma cualquier archivo o directorio: sólo debe haber imágenes en dicho directorio.
			
			baseName = removeFileExtension(it.getName());
			
			results <<	[
							url: g.createLink(controller:'image', action:'picture', params:[imageName: baseName + "." + imagesExtension, idAlojamiento: idAlojamiento]),
							thumbnail_url: g.createLink(controller:'image', action:'thumbnail', params:[imageName: baseName + "." + thumbnailsExtension, idAlojamiento: idAlojamiento]),
							name: baseName
						]
			}
		}
		
		// Si no tiene fotos pone una por defecto	
		if (results.size() == 0) {			
			results.add(defaultImage(idAlojamiento))
		}

		[imagesAlojamientoData: results]
	}
	
	/**
	 * TODO Añade una imagen dependiendo de la localidad en que esté
	 * el alojamiento
	 * @param idAlojamiento
	 * @return
	 */
	def defaultImage(idAlojamiento)	{		
		def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()

		def localidad = Alojamiento.get(idAlojamiento).localidad.localidad.toLowerCase();
					
		if (localidad.contains("cádiz") || localidad.contains("cadiz")) {
			return [
				url: g.resource(dir: 'images', file: 'default_cadiz.jpg'),
				thumbnail_url: g.resource(dir: 'images', file: 'default_cadiz.jpg'),
				name: messageSource.getMessage("defaultimage.cadiz", null, null, null)
			]
		}
		
		else if (localidad.equals("jerez de la frontera")) {
			return [
				url: g.resource(dir: 'images', file: 'default_jerez.jpg'),
				thumbnail_url: g.resource(dir: 'images', file: 'default_jerez.jpg'),
				name: messageSource.getMessage("defaultimage.jerez", null, null, null)
			]
		}
		
		else if (localidad.equals("algeciras")) {
			return [
				url: g.resource(dir: 'images', file: 'default_algeciras.jpg'),
				thumbnail_url: g.resource(dir: 'images', file: 'default_algeciras.jpg'),
				name: messageSource.getMessage("defaultimage.algeciras", null, null, null)
			]
		}
		else if (localidad.equals("puerto real")) {
			return [
				url: g.resource(dir: 'images', file: 'default_puertoreal.jpg'),
				thumbnail_url: g.resource(dir: 'images', file: 'default_puertoreal.jpg'),
				name: messageSource.getMessage("defaultimage.puertoreal", null, null, null)
			]
		}
		
		else {
			return [
				url: g.resource(dir: 'images', file: 'default_resto.jpg'),
				thumbnail_url: g.resource(dir: 'images', file: 'default_resto.jpg'),
				name: messageSource.getMessage("defaultimage.resto", null, null, null)
			]
		}
		
		

	}
}
