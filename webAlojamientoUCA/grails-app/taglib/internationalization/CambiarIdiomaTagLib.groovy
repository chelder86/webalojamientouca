package internationalization

class CambiarIdiomaTagLib {

	/**
	 * Requiere un valor para el atributo: idioma, y la imagen de la bandera.
	 * Ejemplo, para messages_zh_CN.properties: idioma="zh_CN" y la imagen: web-app\images\banderas\zh_CN.png
	 */
	def cambiarIdioma = { attrs ->

		def parametros = [:];
		params.each() { key, value ->
			if (key!="controller" && key!="action" && key!="id" && key!="lang") {
				parametros.put(key, "\'" + value.toString() + "\'");
			}
		};
		parametros.put("lang", "${attrs.idioma}");


		// ¿TODO? Se podrían evitar código repetido, aunque quizás a coste de velocidad: http://stackoverflow.com/questions/17478967/how-to-convert-a-string-into-a-piece-of-code-factory-method-pattern
		// Nota: se intentó implementar con g.link(url:[map]): http://jira.grails.org/browse/GRAILS-10495
		if (params.id != null) {
			out << g.link( id:"${params.id}", controller:"${params.controller}", action:"${params.action}", params:parametros,
			{ g.img( dir:"images/banderas", file:"${attrs.idioma}.png", alt:"${attrs.idioma}") } )
		}

		else {
			out << g.link( controller:"${params.controller}", action:"${params.action}", params:parametros,
			{ g.img( dir:"images/banderas", file:"${attrs.idioma}.png", alt:"${attrs.idioma}") } )
		}
	}



}


