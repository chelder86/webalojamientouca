
/**
 * Necesario para que funcione la internacionalización: http://stackoverflow.com/questions/18818510/does-grails-internationalization-work-in-index-gsp/
 * @author ch
 *
 */
class IndexController {

    def index() { }
}
