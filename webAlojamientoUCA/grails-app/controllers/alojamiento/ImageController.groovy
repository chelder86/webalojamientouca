package alojamiento

import grails.converters.JSON
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

class ImageController {

	def uploadImageService
	def usuarioService
	
    def index() { redirect uri:'/' }
		
	
	/**
	 * id es el id del Alojamiento
	 * @return
	 */
	def add() {		
		
		def alojamientoInstance = Alojamiento.get(params.id)
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			redirect action: 'list'
			return
		}
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(alojamientoInstance.usuario.id)
		if (permisos.modificacionPermitida == false) {
			redirect(controller: "usuario", action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}
		
		[id: params.id]
	}
	
	/**
	 * Basado en el ejemplo de: github.com/sarbogast/grails-bootstrap-file-upload/blob/master/README.md
	 * @return
	 */
	def uploadImage() {
		

		def alojamientoInstance = Alojamiento.get(params.idAlojamiento)
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.idAlojamiento
			])
			redirect action: 'list'
			return
		}
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(alojamientoInstance.usuario.id)
		if (permisos.modificacionPermitida == false) {
			redirect(controller: "usuario", action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}
		
				
		
		String baseName;
		String imageExtension = uploadImageService.imagesExtension;
		String thumbnailExtension = uploadImageService.thumbnailsExtension;
		
		switch(request.method){	
					
			case "GET":			
				def results = []							
				String imagesDirectoryPath = uploadImageService.getImageDirectoryDestinationPath(params.idAlojamiento);
				def dir = new File(imagesDirectoryPath)
				
				if (dir.exists()) {
					dir.eachFile { // Toma cualquier archivo o directorio: sólo debe haber imágenes en dicho directorio.
					
					baseName = uploadImageService.removeFileExtension(it.getName());

					results <<	[
									name: baseName,
									size: it.length(),
									url: createLink(controller:'image', action:'picture', params:[imageName: baseName + "." + imageExtension, idAlojamiento: params.idAlojamiento]),
									thumbnail_url: createLink(controller:'image', action:'thumbnail', params:[imageName: baseName + "." + thumbnailExtension, idAlojamiento: params.idAlojamiento]),
									delete_url: createLink(controller:'image', action:'deleteImage', params:[baseName: baseName, idAlojamiento: params.idAlojamiento]),
									delete_type: "DELETE"
								]
					}
				}
				render results as JSON
				break;
				
			case "POST":
				def results = []
				if (request instanceof MultipartHttpServletRequest){
				
					for(filename in request.getFileNames()){
						MultipartFile file = request.getFile(filename)
						BufferedImage originalImage = ImageIO.read(file.getInputStream());
						baseName = uploadImageService.removeFileExtension(file.getOriginalFilename());
						
						// Guardamos la imagen original:
						uploadImageService.saveImage(originalImage, params.idAlojamiento, baseName + "." + imageExtension)
						
						// Guardamos una miniatura de la imagen original:
						uploadImageService.saveThumbnail(originalImage, params.idAlojamiento, baseName + "." + thumbnailExtension)
							
						results <<	[
										name: baseName,
										size: file.getSize(),
										url: createLink(controller:'image', action:'picture', params:[imageName: baseName + "." + imageExtension, idAlojamiento: params.idAlojamiento]),
										thumbnail_url: createLink(controller:'image', action:'thumbnail', params:[imageName: baseName + "." + thumbnailExtension, idAlojamiento: params.idAlojamiento]),
										delete_url: createLink(controller:'image', action:'deleteImage', params:[baseName: baseName, idAlojamiento: params.idAlojamiento]),
										delete_type: "DELETE"
									]						
					}
					
					// Actualiza el lastUpdated de Alojamiento a la fecha actual:
					// def alojamientoInstance = Alojamiento.get(params.idAlojamiento) //< Ya lo hacemos arriba
					params.putAt('lastUpdated', new Date())
					alojamientoInstance.properties = params
					alojamientoInstance.save(flush: true)
					
				}

				render results as JSON
				break;
			default: render status: HttpStatus.METHOD_NOT_ALLOWED.value()
		}
	}

	
	def picture(){
		
		String imageDestinationPath = uploadImageService.getImageDestinationPath(params.idAlojamiento, params.imageName)
		File picFile = new File(imageDestinationPath)
		response.contentType = 'image/jpeg'
		response.outputStream << new FileInputStream(picFile)
		response.outputStream.flush()
		
	}

	
	def thumbnail(){
		
		String thumbnailDestinationPath = uploadImageService.getThumbnailDestinationPath(params.idAlojamiento, params.imageName)
		File picFile = new File(thumbnailDestinationPath)
		response.contentType = 'image/jpeg'
		response.outputStream << new FileInputStream(picFile)
		response.outputStream.flush()
		
	}
	

	def deleteImage(){	
		
		
		def alojamientoInstance = Alojamiento.get(params.idAlojamiento)
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.idAlojamiento
			])
			redirect action: 'list'
			return
		}
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(alojamientoInstance.usuario.id)
		if (permisos.modificacionPermitida == false) {
			redirect(controller: "usuario", action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}
			
		
		String imageName = params.baseName + "." + uploadImageService.imagesExtension;
		String thumbnailName = params.baseName + "." + uploadImageService.thumbnailsExtension;
		File image = new File (uploadImageService.getImageDestinationPath(params.idAlojamiento, imageName));
		File thumbnail = new File (uploadImageService.getThumbnailDestinationPath(params.idAlojamiento, thumbnailName));
		
		//System.gc() //-> Truco: descomentar si thumbail.delete() no lo borra; aunque parece que eso ocurre únicamente en Eclipse.
		thumbnail.delete()
		image.delete()
		def result = [success: true]
		render result as JSON
		
	}
	


	

	
	
	
	
}
