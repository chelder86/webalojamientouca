package alojamiento

import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

class AlojamientoController {
	
	def MessageSource messageSource
	def springSecurityService
	def usuarioService
	def uploadImageService
	
	static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

	// Las ciudades sin messageSource... debe aparecer exactamente igual que en la base de datos:
	def listaLocalidadesFiltrable () {
		[
			cadiz : messageSource.getMessage("filtrarAlojamiento.filtroLocalidad.cadiz", null, null, null),
			cadizCentro : "Cádiz: centro",
			cadizParteNueva : "Cádiz: parte nueva",
			jerez : "Jerez de la Frontera",
			algeciras : "Algeciras",
			puertoReal : "Puerto Real",
			resto : messageSource.getMessage("filtrarAlojamiento.filtroLocalidad.resto", null, null, null)
		]
	}
	
	/**
	 * Reduce la precisión de las coordenadas a 5 dígitos decimales. De no hacerlo pasan cosas raras.
	 * @param coordenada
	 * @return
	 */
	private def reducirPrecision(coordenada) {
		coordenada = Float.parseFloat(coordenada);
		return ( (float) ( (int) coordenada * 100000 ) / 100000 );		
	}
	
	
	def index() {
		redirect action: 'list', params: params
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
				
		if (params.sort == null) {			
			params.sort = "lastUpdated"
			params.order = "desc"
		}
		
		def defaultList = Alojamiento.findAllByNumeroPlazasLibresGreaterThan(0, params)
				
		[alojamientoInstanceList: defaultList, alojamientoInstanceTotal: defaultList.size(),
			listaLocalidadesFiltrable: listaLocalidadesFiltrable()]
	}
	
	
	def userList(){
		
		def usuario = getAuthenticatedUser()
		def alojamientosUsuarioLogueado = Alojamiento.findAllByUsuario(usuario)
		
		// Para que añada un espacio entre el index.gsp y el pie de página.		
		if (alojamientosUsuarioLogueado.isEmpty()) {
			render "&nbsp; <br> <br> <br> <br> "			
		}
		
		else {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)			
		[alojamientoInstanceList: alojamientosUsuarioLogueado, alojamientoInstanceTotal: alojamientosUsuarioLogueado.size()]
		}
		
	}
	

	def create() {
		switch (request.method) {
			case 'GET':			
				[alojamientoInstance: new Alojamiento(params)]
				break
			case 'POST':
			
				// Asignamos el alojamiento al usuario que lo crea:
				def usuarioInstance = getAuthenticatedUser()
				params.putAt('usuario', usuarioInstance)
				
				// Reducimos la precisión de las coordenadas:
				params.putAt('latitud_norte', reducirPrecision(params.latitud_norte))
				params.putAt('longitud_oeste', reducirPrecision(params.longitud_oeste))
				
				def alojamientoInstance = new Alojamiento(params)
				if (!alojamientoInstance.save(flush: true)) {
					render view: 'create', model: [alojamientoInstance: alojamientoInstance]
					return
				}

				flash.message = message(code: 'default.created.message', args: [
					message(code: 'alojamiento.label'),
					alojamientoInstance.id
				])
				redirect action: 'show', id: alojamientoInstance.id
				break
		}
	}

	def show() {
		def alojamientoInstance = Alojamiento.get(params.id)
		
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			redirect action: 'list'
			return
		} 		
		
		def imagesAlojamientoData = uploadImageService.getImagesForCarousel(params.id)
		
		[alojamientoInstance: alojamientoInstance, imagesAlojamientoData: imagesAlojamientoData, mostrarBotonesEdicion: usuarioService.comprobarPermisosDeVisualizacion(alojamientoInstance.usuario.id).visualizacionPermitida]
	}

	
	
	
	def edit() {
		
		def alojamientoInstance = Alojamiento.get(params.id)
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			redirect action: 'list'
			return
		}
		
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(alojamientoInstance.usuario.id)
		if (permisos.modificacionPermitida == false) {
			redirect(controller: "usuario", action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}


		switch (request.method) {
			case 'GET':
				[alojamientoInstance: alojamientoInstance]
				break
			case 'POST':
				if (params.version) {
					def version = params.version.toLong()
					if (alojamientoInstance.version > version) {
						alojamientoInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
								[
									message(code: 'alojamiento.label')] as Object[],
								"Another user has updated this Alojamiento while you were editing")
						render view: 'edit', model: [alojamientoInstance: alojamientoInstance]
						return
					}
				}

				// Reducimos la precisión de las coordenadas:
				params.putAt('latitud_norte', reducirPrecision(params.latitud_norte))
				params.putAt('longitud_oeste', reducirPrecision(params.longitud_oeste))
									
				alojamientoInstance.properties = params

				if (!alojamientoInstance.save(flush: true)) {
					render view: 'edit', model: [alojamientoInstance: alojamientoInstance]
					return
				}

				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'alojamiento.label'),
					alojamientoInstance.id
				])
				redirect action: 'show', id: alojamientoInstance.id
				break
			}
		
	}

	def delete() {

		
		def alojamientoInstance = Alojamiento.get(params.id)
		if (!alojamientoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			redirect action: 'list'
			return
		}
		
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(alojamientoInstance.usuario.id)
		if (permisos.modificacionPermitida == false) {
			redirect(controller: "usuario", action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}



		try {
			alojamientoInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			
			//Borra también las imágenes asociadas:
			uploadImageService.deleteImageAndThumbnailDirectories(params.id)
			
			redirect uri:'/', id: params.id
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [
				message(code: 'alojamiento.label'),
				params.id
			])
			redirect action: 'show', id: params.id
		}
		
	}
	
	
	def search() {
		
		def c = Alojamiento.createCriteria()
		def results = c.list (params) {

			if (!params.filtrarNumeroMinimoDePlazasLibres.isEmpty()) {
				ge("numeroPlazasLibres", params.filtrarNumeroMinimoDePlazasLibres.toInteger())

			}

			if (!params.filtrarPrecioMaximoPorPlaza.isEmpty()) {
				le("precioPorPlazaIndividual", params.filtrarPrecioMaximoPorPlaza.toFloat())
			}

			if (!params.filtrarLocalidad.isEmpty()) {

				localidad {
					if (params.filtrarLocalidad.equals("cadiz")) {
						or{
							eq("localidad", listaLocalidadesFiltrable().get("cadizCentro",""))
							eq("localidad", listaLocalidadesFiltrable().get("cadizParteNueva",""))
						}
					}
					else if (params.filtrarLocalidad.equals("resto")) {
						not {
							'in'("localidad",[
								listaLocalidadesFiltrable().get("cadizCentro",""),
								listaLocalidadesFiltrable().get("cadizParteNueva",""),
								listaLocalidadesFiltrable().get("jerez",""),
								listaLocalidadesFiltrable().get("algeciras",""),
								listaLocalidadesFiltrable().get("puertoReal","")
							]) 
						}
					}
					else {
						eq("localidad", listaLocalidadesFiltrable().get(params.filtrarLocalidad,""))
					}
				}
			}
			if (params.containsKey("sort"))
				order(params.sort, params.order)
			else
				order("lastUpdated", "desc")
		}
		render(view: "list", model: [alojamientoInstanceList: results, alojamientoInstanceTotal: results.size(), listaLocalidadesFiltrable: listaLocalidadesFiltrable()])
	}

	/**
	 * Necesario para que funcione la internacionalización: http://stackoverflow.com/questions/18818510/does-grails-internationalization-work-in-index-gsp/
	 * @return
	 */
	def contacto() {}
}
