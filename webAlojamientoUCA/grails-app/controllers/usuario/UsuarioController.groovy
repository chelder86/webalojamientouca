package usuario


import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils



/**
 *  Verifica que el password introducido es correcto e igual al introducido en confirmar password:
 * @author ch
 *
 */
@grails.validation.Validateable
class CheckPasswordCommand {
	String password
	String confirmarPassword

	static constraints = {
		password size:5..15
		confirmarPassword validator: { val, obj ->
			if ((val != obj.password)) {
				return 'usuario.password.dontmatch'
			}
			return true
		}
	}
}


@grails.validation.Validateable
class ChangePasswordCommand {
	
	def springSecurityService;
	
	String password
	String nuevoPassword
	String confirmarNuevoPassword

	static constraints = {

		password (size:5..15, validator: {String password, obj ->
			String enc = obj.springSecurityService.encodePassword(password)
			String encPasswordUser = obj.springSecurityService.currentUser.password
			//check if the actual password informed is not equals as the current user password
			if( enc != encPasswordUser ) {
				return 'ChangePasswordCommand.currentPasswordMismatch' //key of the i18 message
			} else {
				return true
			}
		})
		
		nuevoPassword (size:5..15)
		
		confirmarNuevoPassword (size:5..15, validator: {String confirmarNuevoPassword, obj ->
			String nuevoPassword = obj.properties['nuevoPassword']
			if( !(nuevoPassword == confirmarNuevoPassword) ) {
				return 'ChangePasswordCommand.newPasswordMismatch' //key of the i18 message
			} else {
				return true
			}
		})
		
	}
}



class UsuarioController {

	def springSecurityService
	def usuarioService
	
	static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

	
	def index() {
		// Redirige al show del usuario logueado
		def usuarioInstance = getAuthenticatedUser()
		params.putAt('id', usuarioInstance.id)
		redirect action: 'show', params: params
	}

	/**
	 * Si se intenta alguna de las acciones no permitidas por falta de permisos, se informa al usuario
	 * @return
	 */
	def notAllowedInformation() {		
		[titleText: params.titleText, descriptionText: params.descriptionText]
	}
	
		
	def list() {		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeVisualizacion(-1)
		if (permisos.visualizacionPermitida == false) {
			redirect(action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}

		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[usuarioInstanceList: Usuario.list(params), usuarioInstanceTotal: Usuario.count()]
	}

	def create(CheckPasswordCommand checkPasswordCommand) {

		switch (request.method) {
			case 'GET':
				[usuarioInstance: new Usuario(params)]
				break
			case 'POST':
				def usuarioInstance = new Usuario(params)

				// Si el el object command ha detectado restricciones que no se cumplen, las muestra:
				if (checkPasswordCommand.hasErrors()) {
					render view: 'create', model: [usuarioInstance: usuarioInstance, checkPasswordCommand: checkPasswordCommand]
					return
				}

				if (!usuarioInstance.save(flush: true)) {
					render view: 'create', model: [usuarioInstance: usuarioInstance]
					return
				}

			
				// Asigna el rol de usuario a todo usuario nuevo creado:
				def userRole = Rol.findByAuthority("ROL_USUARIO")
				UsuarioRol.create usuarioInstance, userRole

				flash.message = message(code: 'default.created.message', args: [
					message(code: 'usuario.label'),
					usuarioInstance.id
				])
				
				//TODO redirigir postCreate mejor
				//redirect action: 'postCreate'
				redirect uri:'/', id: params.id 
				break

		}
	}
	
	/**
	 * TODO Información útil para el usuario. También se enviará al email.
	 * @return
	 */
	def postCreate() {

	}

	def show() {
		
		def usuarioInstance = Usuario.get(params.id)
		if (!usuarioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'usuario.label'),
				params.id
			])
			redirect action: 'list'
			return
		}
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeVisualizacion(params.id)
		if (permisos.visualizacionPermitida == false) {
			redirect(action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}

		[usuarioInstance: usuarioInstance]
	}

	def edit() {
		
		def usuarioInstance = Usuario.get(params.id)
		if (!usuarioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'usuario.label'),
				params.id
			])
			redirect action: 'list'
			return
		}

		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(params.id)		
		if (permisos.modificacionPermitida == false) {
			redirect(action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}

		switch (request.method) {
			case 'GET':
				// Si es superadministrador permite cambiar roles:
				if (SpringSecurityUtils.ifAllGranted('ROL_SUPERADMIN')){					
					// Pasa a la vista el rol actual del usuario:
					def rolActualId = UsuarioRol.findByUsuario(usuarioInstance).rol.id					
					[usuarioInstance: usuarioInstance, rolActualId: rolActualId]
				}

				else {
					[usuarioInstance: usuarioInstance]
				}

				break
				
			case 'POST':
				// Si es superadministrador permite cambiar roles:
				if (SpringSecurityUtils.ifAllGranted('ROL_SUPERADMIN')){

					def rolActualId = UsuarioRol.findByUsuario(usuarioInstance).rol.id
					Integer rolNuevoId = params.selectorRol.toInteger()
					if (rolActualId != rolNuevoId) {						
						def rolInstance = Rol.findById(rolNuevoId)
						usuarioService.updateRol(usuarioInstance, rolInstance)					
						//TODO Hace que el cambio de rol tenga efecto inmediato:
						//usuarioService.recargarSistemaConNuevoRol() 
					}
				}

				if (params.version) {
					def version = params.version.toLong()
					if (usuarioInstance.version > version) {
						usuarioInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
								[
									message(code: 'usuario.label')] as Object[],
								"Another user has updated this Usuario while you were editing")
						render view: 'edit', model: [usuarioInstance: usuarioInstance]
						return
					}
				}

				usuarioInstance.properties = params

				if (!usuarioInstance.save(flush: true)) {
					render view: 'edit', model: [usuarioInstance: usuarioInstance]
					return
				}

				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'usuario.label'),
					usuarioInstance.id
				])
				redirect action: 'show', id: usuarioInstance.id
				break
		}
	}

	def delete() {
		
		def usuarioInstance = Usuario.get(params.id)
		if (!usuarioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'usuario.label'),
				params.id
			])
			redirect uri:'/', id: params.id
			return
		}

		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(params.id)		
		if (permisos.modificacionPermitida == false) {
			redirect(action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}
			

		
		try {
			usuarioService.deleteUser(usuarioInstance)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [
				message(code: 'usuario.label'),
				params.id
			])
			redirect action: 'show', id: params.id
		}
					
		// TODO redirigir a postDeleteMejor
		// redirect action: 'postDelete'
		flash.message = message(code: 'default.deleted.message', args: [
			message(code: 'usuario.label'),
			params.id
		])	
		redirect uri:'/', id: params.id
		
	}
	
	/**
	 * TODO
	 * @return
	 */
	def changePassword(ChangePasswordCommand changePasswordCommand) {
		
		// Si el usuario no tiene los permisos necesarios, lo informa:
		def permisos = usuarioService.comprobarPermisosDeModificacion(params.id)
		if (permisos.modificacionPermitida == false) {
			redirect(action: "notAllowedInformation", params: [titleText: permisos.titleText, descriptionText: permisos.descriptionText])
			return
		}

		switch (request.method) {
			case 'GET':
				def usuarioInstance = Usuario.get(params.id)
				if (!usuarioInstance) {
					flash.message = message(code: 'default.not.found.message', args: [
						message(code: 'usuario.label'),
						params.id
					])
					redirect controller: 'usuario'
					return
				}		
				[usuarioInstance: usuarioInstance]
				break
				
			case 'POST':
				def usuarioInstance = Usuario.get(params.id)
				if (!usuarioInstance) {
					flash.message = message(code: 'default.not.found.message', args: [
						message(code: 'usuario.label'),
						params.id
					])
					redirect controller: 'usuario'
					return
				}

				if (params.version) {
					def version = params.version.toLong()
					if (usuarioInstance.version > version) {
						usuarioInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
								[
									message(code: 'usuario.label')] as Object[],
								"Another user has updated this Usuario while you were editing")
						render view: 'changePassword', model: [usuarioInstance: usuarioInstance]
						return
					}
				}

				usuarioInstance.properties = params

				// Si el el object command ha detectado restricciones que no se cumplen, las muestra:
				if (changePasswordCommand.hasErrors()) {
					render view: 'changePassword', model: [usuarioInstance: usuarioInstance, changePasswordCommand: changePasswordCommand]
					return
				}
				
				// Codifica el password antes de guardarlo:
				usuarioInstance.password = params.nuevoPassword;
				
				
				if (!usuarioInstance.save(flush: true)) {
					render view: 'changePassword', model: [usuarioInstance: usuarioInstance]
					return
				}
				
				// Reautentifica al usuario con la nueva contraseña
				if (springSecurityService.loggedIn &&
					springSecurityService.principal.username == usuarioInstance.username) {
						springSecurityService.reauthenticate usuarioInstance.username
				}

				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'usuario.label'),
					usuarioInstance.id
				])
				redirect action: 'show', id: usuarioInstance.id			
				break
		}
		
		
				
	}
	
	
	/**
	 * TODO Informa al usuario de que el usuario ha sido eliminado.
	 *  Informa que todavía puede navegar hasta que haga logout
	 * @return
	 */
	def postDelete() {	
	}
	
	def allEmails() {
		if ( SpringSecurityUtils.ifAnyGranted("ROL_SUPERADMIN") ) {
			def usuario = usuario.Usuario.list()
			render ("${usuario.email}")
		}
		else{
			redirect uri:'/'
		}
	}
	
}
