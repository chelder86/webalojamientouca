package usuario

class Usuario {

	transient springSecurityService

	String username
	String password
	boolean enabled = Boolean.TRUE
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	
	/*
	 * Propiedades que se han añadido (a las por defecto 
	 * creadas con grails s2-quickstart):
	 */
	//String confirmarPassword -> se hace en un command object en UsuarioController: http://stackoverflow.com/questions/18370593/spring-security-core-and-custom-validator-for-a-confirm-password-field-is-possib/18374315#18374315
	String nombre
	String apellidos
	String email
	String telefono
	String documentoIdentificacion
	boolean aceptarPoliticaDeProteccionDeDatos = Boolean.FALSE
	
	
	// Los siguientes valores no se almacenarán en la base de datos:
	static transients = ['aceptarPoliticaDeProteccionDeDatos']
	
	
	static constraints = {
		username blank: false, unique: true
		password blank: false
		
		// Restricciones añadidas:		
		nombre blank: false
		apellidos blank: false
		email blank: false, email:true
		telefono blank: false, matches: "[0-9 -+()]{3,15}"
		documentoIdentificacion blank: false
		aceptarPoliticaDeProteccionDeDatos bindable: true, validator:{ val, obj ->
			if (!obj.id && obj.aceptarPoliticaDeProteccionDeDatos != true) //> !obj.id = el usuario no existe todavía. Si ya está creado es que lo está modificando y no es necesario que vuelva a aceptar la política.
				return 'usuario.politicaprotecciondatos.notaccepted'
		}
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Rol> getAuthorities() {
		UsuarioRol.findAllByUsuario(this).collect { it.rol } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
