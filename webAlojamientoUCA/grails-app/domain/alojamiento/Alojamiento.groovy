package alojamiento

import usuario.Usuario
import java.util.Date;



class Alojamiento implements Serializable {

	Localidad localidad
	String direccion
	Float latitud_norte = 0
	Float longitud_oeste = 0
	Float precioPorPlazaIndividual
	Integer numeroPlazasTotales
	Integer numeroPlazasLibres	
	Boolean permiteAlquilarPlazasSueltas = Boolean.FALSE
	String datosContacto
	String observaciones
	Usuario usuario	
	Date lastUpdated
	
	// Caracteristicas
	Integer numeroAseos
	Boolean tieneAscensor = Boolean.FALSE
	Boolean tieneTelevisor = Boolean.FALSE
	Boolean tieneLavadora = Boolean.FALSE
	Boolean tieneMicroondas = Boolean.FALSE
	Boolean tieneHorno = Boolean.FALSE
	Boolean tieneLavavajillas = Boolean.FALSE
	Boolean tieneSecadora = Boolean.FALSE
	Boolean tieneFrigorifico = Boolean.FALSE
	Boolean tieneCongelador = Boolean.FALSE
	Boolean tieneMuebles = Boolean.FALSE
	Boolean tieneAireAcondicionado = Boolean.FALSE
	Boolean tieneCalefaccion = Boolean.FALSE
	
	// Es apto para:
	Boolean esAptoParaMinusvalidos = Boolean.FALSE
	
	// Permite:
	Boolean permiteErasmus = Boolean.FALSE
	Boolean permiteChicos = Boolean.FALSE
	Boolean permiteChicas = Boolean.FALSE
	Boolean permiteFumadores = Boolean.FALSE
	Boolean permiteAnimales = Boolean.FALSE
	
	// Precio incluye:
	Boolean precioIncluyeGaraje = Boolean.FALSE
	Boolean precioIncluyeTelefono = Boolean.FALSE
	Boolean precioIncluyeInternet = Boolean.FALSE
	Boolean precioIncluyeAgua = Boolean.FALSE
	Boolean precioIncluyeElectricidad = Boolean.FALSE
	Boolean precioIncluyeComunidad = Boolean.FALSE
	Boolean precioIncluyeGas = Boolean.FALSE
	Boolean precioIncluyeLimpieza = Boolean.FALSE
	Boolean precioIncluyeDesayuno = Boolean.FALSE
	Boolean precioIncluyeAlmuerzo = Boolean.FALSE
	Boolean precioIncluyeCena = Boolean.FALSE
	
	


	static constraints = {
		localidad blank: false
		direccion maxSize: 100, blank: false
		datosContacto maxSize: 450, blank: false
		observaciones nullable: true, maxSize: 450
		lastUpdated display: false
		usuario display:false		
	}
}

