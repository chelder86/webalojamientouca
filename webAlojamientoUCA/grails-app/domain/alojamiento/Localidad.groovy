package alojamiento


class Localidad implements Serializable {

	
	Integer id
	String localidad

	
	static hasMany = [alojamientos: Alojamiento]

	
	static constraints = {
		localidad blank: false, maxSize: 45
	}
	
	
}
