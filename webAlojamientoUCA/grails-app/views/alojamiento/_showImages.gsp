


<%-- Twitter Bootstrap Carousel --%>

<div>

	<div id="mini-carousel-photos" class="carousel slide centrar-imagen">
		<div class="carousel-inner">

			<g:each in="${imagesAlojamientoData}">
				<g:each var="image" in="${it.value}">


					<div class="item">		
						<div class="centrar-imagen">			
							<a href="${image.url}"> 
								<img src="${image.thumbnail_url}" alt="${image.name}"> 
							</a>
						</div>	
						<div class="carousel-caption">
							<h4> ${image.name} </h4>
						</div>						
					</div>

				</g:each>
			</g:each>
		</div>
		<a class="left carousel-control" href="#mini-carousel-photos" data-slide="prev">&lsaquo;</a>
		<a class="right carousel-control" href="#mini-carousel-photos" data-slide="next">&rsaquo;</a>
	</div>

<%-- Añade a la última imagen del carousel "active", quedando class="item active": --%>
<script type="text/javascript">
	$(".item:last").addClass('active');
</script>

<%--Para que empieze a cambiar de imagen automáticamente--%>
<script type="text/javascript">
  $(document).ready(function() {
        $('.carousel').carousel({
  			interval: 5000
  		})
  });
</script>

</div>