							<div id=camposGmaps class="span11 offset1">	
								
								<div class="row-fluid">					
									<f:field bean="alojamientoInstance" property="localidad" input-optionValue="localidad" input-onkeydown="return checkIfEnterPushed(event, 'botongmapbuscardireccion')"/>
									<f:field bean="alojamientoInstance" property="direccion" input-class="input-xxlarge" input-onkeydown="return checkIfEnterPushed(event, 'botongmapbuscardireccion')"/>
									
									<p class="help-block offset2 margenesAyuda">
										<i class="icon-pencil"></i>
										<g:message code="alojamiento.botongmapbuscardireccion.ayuda" />
									</p>
									
									<button id="botongmapbuscardireccion" type='button' class="btn btn-primary offset1" onclick="codeAddress()">
										<g:message code="alojamiento.botongmapbuscardireccion" />  &#62;&#62;
									</button>
									
								</div>
								
								<br>
								
								<div id="map-canvas" class="addBordes span6" style="width: 400px; height: 300px"></div>
																		
								<div class="addBordes span6">	
									<br>													
									<g:checkBox name="establecerCoordenadasManualmente" value="${false}" onclick="checkbuttonCoordenadasManualClick()"/>
									<g:message code="alojamiento.coordenadasmanual" />		
									
									<p class="help-block margenesAyuda offset1">
										<i class="icon-pencil"></i>
										<g:message code="alojamiento.coordenadasmanual.ayuda" />
									</p>									
																									
									<f:field bean="alojamientoInstance" property="latitud_norte" input-onkeydown="return checkIfEnterPushed(event, 'botonCoordenadas')"/>										
									<f:field bean="alojamientoInstance" property="longitud_oeste" input-onkeydown="return checkIfEnterPushed(event, 'botonCoordenadas')"/>
									
									<button id="botonCoordenadas" type='button' class="btn" onclick="dibujarCoordenadasEnMapa()">
										&#60;&#60;  <g:message code="alojamiento.botongmapbuscarcoordenadas" />
									</button>	
									
																
								</div>

							</div>
							
							<div class="row-fluid">

								<div class="span11 offset1">	
																<br>
								<hr>
								<br>
									<div class="misma-linea-contenedor">
										<div class="misma-linea-A">
											<f:field bean="alojamientoInstance" property="precioPorPlazaIndividual" input-class="input-mini"/>
										</div>									
										<div class="help-block misma-linea-B">
											<i class="icon-pencil"></i>
											<g:message code="alojamiento.precioPorPlazaIndividual.ayuda" />
										</div>	
									</div>
									
									<f:field bean="alojamientoInstance" property="numeroPlazasTotales" input-class="input-mini"/>
									<f:field bean="alojamientoInstance" property="numeroPlazasLibres" input-class="input-mini"/>
									
									<div class="misma-linea-contenedor">
										<div class="misma-linea-A-forzada">									
											<f:field bean="alojamientoInstance" property="permiteAlquilarPlazasSueltas"/>
										</div>									
										<div class="help-block misma-linea-B-forzada30">											
											<i class="icon-pencil"></i>
											<g:message code="alojamiento.permiteAlquilarPlazasSueltas.ayuda" />												
										</div>	
									</div>	
									
									<%-- Si se está creando un nuevo alojamiento, añade los datos de contacto del usuario logueado:--%>
									<g:if test="${params.action == 'create'}">
									     <g:set var="createDefaultValue">${usuario.Usuario.findByUsername(sec.loggedInUserInfo(field:'username')).nombre}: ${usuario.Usuario.findByUsername(sec.loggedInUserInfo(field:'username')).telefono},  ${usuario.Usuario.findByUsername(sec.loggedInUserInfo(field:'username')).email}</g:set>
									</g:if>
									<g:else>
									     <g:set var="createDefaultValue">${alojamientoInstance.datosContacto}</g:set>
									</g:else>																			
									<f:field  bean="alojamientoInstance" property="datosContacto">
										<g:textArea name="datosContacto" cols="150" rows="3" maxlength="450" style="width: 500px; height: 150px;" value="${createDefaultValue}"/>
									</f:field>
							
									<div id=restoDeCampos>
																		
										<br>
										<br>
										<h5> <g:message code="alojamiento.checkboxs.titulo" /> </h5>
										<br>
										<f:field bean="alojamientoInstance" property="numeroAseos" input-class="input-mini"/>	
										<f:all bean="alojamientoInstance" except="localidad, direccion, latitud_norte, longitud_oeste, precioPorPlazaIndividual, numeroPlazasTotales, numeroPlazasLibres, permiteAlquilarPlazasSueltas, datosContacto, numeroAseos, observaciones"/>
										<f:field bean="alojamientoInstance" property="observaciones">
											<g:textArea name="observaciones" cols="150" rows="3" maxlength="450" style="width: 500px; height: 150px;" value="${alojamientoInstance.observaciones}"/>
										</f:field>
									</div>
								</div>			
								</div>	