<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'alojamiento.label')}" />
		<title><g:message code="contacto.title" /></title>
	</head>
	<body>
	
		<script type="text/javascript">
			$('.linea-horizontal').css({"display": "none"});
			$('#contacto').attr('class','active')
		</script>	
	
		<div class="row-fluid">
			
			<div class="span7 offset2 text-right">
			
				
				<br>
				<br>
				
				<h4>		
					<g:message code="contacto.informacioncontacto" /> 		
					<a href="http://www.erasmuscadiz.com/es/alojamiento">erasmuscadiz.com/alojamiento</a>
				</h4>
				
				<p class="help-block">
					<i class="icon-pencil"></i>
					<g:message code="contacto.informacioncontacto.ayuda" />
				</p>				
				
				<br>
				
				<h4>		
					<g:message code="contacto.avisoerroresweb" /> 		
					<a href="https://bitbucket.org/chelder86/webalojamientouca/issues?status=new&status=open">bitbucket.org/chelder86/webalojamientouca/issues</a>
				</h4>
				
				<p class="help-block">
					<i class="icon-pencil"></i>
					<g:message code="contacto.avisoerroresweb.ayuda" />
				</p>
				
				<br>
				<br>
				<br>
				
			</div>

		</div>
	</body>
</html>
