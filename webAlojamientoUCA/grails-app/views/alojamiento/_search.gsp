

<h4>
	<g:message code="search.titulo" />
</h4>

<g:form name="myForm" action="search">


	<div class="control-group">
		<h5>Localidad:</h5>
		<g:select class="span9" from="${listaLocalidadesFiltrable.entrySet()}"
			name="filtrarLocalidad" optionKey="key" optionValue="value"
			noSelection="${['':message(code: "search.nofiltrar")]}" />
	</div>


	<div class="control-group">
		<h5>
			<g:message code="search.plazasminimo" />
		</h5>
		<div class="controls">
			<input id="filtrarNumeroMinimoDePlazasLibres"
				name="filtrarNumeroMinimoDePlazasLibres" type="text"
				placeholder="no filtrar" class="input-mini">
			<p class="help-block">
				<i class="icon-pencil"></i>
				<g:message code="search.plazasminimo.ayuda" />
			</p>

		</div>
	</div>


	<div class="control-group">
		<h5>
			<g:message code="search.preciomaximo" />
		</h5>
		<div class="controls">
			<input id="filtrarPrecioMaximoPorPlaza"
				name="filtrarPrecioMaximoPorPlaza" type="text"
				placeholder="no filtrar" class="input-mini">

		</div>
	</div>


	<div class="text-right">
		<g:actionSubmit class="btn btn-danger"
			value="${message(code:'search.botonfiltrar')}"
			action="search" />
	</div>

</g:form>

