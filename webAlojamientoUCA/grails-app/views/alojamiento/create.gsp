<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'alojamiento.label')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
<%--	TODO Añadir el código de https://developers.google.com/maps/documentation/javascript/tutorial?hl=es#api_key (a la espera de que se obtenga con el email de ESN)--%>
		<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
		<jawr:script src="/i18n/messages.js"/>
		<g:javascript src="googlemaps_modificar.js" />
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span12">

				<div class="page-header">
					<h1><g:message code="default.create.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${alojamientoInstance}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${alojamientoInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>

				<fieldset>
					<g:form class="form-horizontal" action="create">
						<fieldset>
						
							<g:render template="form"/>
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">
											<i class="icon-ok icon-white"></i>
											<g:message code="default.button.create.label"/>
										</button>
									</div>
								
							
						</fieldset>
					</g:form>
				</fieldset>
				
			</div>

		</div>
	</body>
</html>
