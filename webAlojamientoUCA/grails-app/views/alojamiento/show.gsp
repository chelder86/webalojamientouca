
<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'alojamiento.label')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
<%--	TODO Añadir el código de https://developers.google.com/maps/documentation/javascript/tutorial?hl=es#api_key (a la espera de que se obtenga con el email de ESN)--%>
		<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
		<g:javascript src="googlemaps_ver.js" />
	</head>
	
	<body>
	
		<g:if test="${mostrarBotonesEdicion}">
		<div class="pull-left situartransparente-capa-superiora-carousel">			
				<g:link controller="image" action="add" id="${alojamientoInstance.id}" class="btn btn-small btn-danger"><g:message code="list.botonfotos" /> &raquo; </g:link>
				<g:link action="edit" id="${alojamientoInstance.id}" class="btn btn-small"><g:message code="list.botoneditaralojamiento" /> &raquo; </g:link>
				
		</div>
		</g:if>
	
	
	
	
	
			
		<div class="row-fluid">
						
			<div class="span12">
				<g:if test="${flash.message}">
					<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<div>								
					<g:render template="showImages"/>				
				</div>
			</div>
			
		</div>
		
			
		<div class="row-fluid">	
		
			<div class="span10 offset2">
				<h3>${alojamientoInstance?.direccion}  <font size="-1"> (${alojamientoInstance?.localidad?.localidad})</font> </h3>				
			</div>
			
		</div>	
			
			
		<div class="row-fluid">	

			<div class="span4">	
				<div class="text-center">						
				<span id="map-canvas" class="addBordes span6" style="width: 100%; height: 300px"></span>
				<span> <g:message code="alojamiento.latitud_norte.label"/>, <g:message code="alojamiento.longitud_oeste.label"/>: </span>
				<span id="latitud_norte">${alojamientoInstance.latitud_norte}</span> <span>, </span> <span id="longitud_oeste">${alojamientoInstance.longitud_oeste}</span>
				</div>				
			</div>
										
			<div class="span4 text-center">								
				<p>
					<g:message code="alojamiento.numeroPlazasLibres.label"/>: 
					<b><font size="+1"> <g:fieldValue bean="${alojamientoInstance}" field="numeroPlazasLibres"/></font></b>
				</p>	
				<p>
					<g:message code="alojamiento.numeroPlazasTotales.label"/>: 
					<b><font size="+1"> <g:fieldValue bean="${alojamientoInstance}" field="numeroPlazasTotales"/></font></b>
				</p>	
				<p>
					<g:message code="alojamiento.precioPorPlazaIndividual.label"/>: 
					<b><font size="+1"> <g:fieldValue bean="${alojamientoInstance}" field="precioPorPlazaIndividual"/></font></b>
				</p>
					
				<hr>
				<h4> <g:message code="alojamiento.datosContacto.label"/>: </h4>
				<p> <g:fieldValue bean="${alojamientoInstance}" field="datosContacto"/></p>									
			</div>
			
			<div class="span4">										
				<p>
					<g:message code="alojamiento.esAptoParaMinusvalidos.label"/> 
					<i class="${alojamientoInstance?.esAptoParaMinusvalidos?'icon-ok':'icon-remove'}" ></i>
				</p>	
				<hr>
				<p>
					<g:message code="alojamiento.permiteErasmus.label"/> 
					<i class="${alojamientoInstance?.permiteErasmus?'icon-ok':'icon-remove'}" ></i>
				</p>	
				<p>
					<g:message code="alojamiento.permiteChicos.label"/> 
					<i class="${alojamientoInstance?.permiteChicos?'icon-ok':'icon-remove'}" ></i>
				</p>										
				<p>
					<g:message code="alojamiento.permiteChicas.label"/> 
					<i class="${alojamientoInstance?.permiteChicas?'icon-ok':'icon-remove'}" ></i>
				</p>	
				<p>
					<g:message code="alojamiento.permiteFumadores.label"/> 
					<i class="${alojamientoInstance?.permiteFumadores?'icon-ok':'icon-remove'}" ></i>
				</p>					
				<p>
					<g:message code="alojamiento.permiteAnimales.label"/> 
					<i class="${alojamientoInstance?.permiteAnimales?'icon-ok':'icon-remove'}" ></i>
				</p>
				<hr>					
				<p>
					<g:message code="alojamiento.permiteAlquilarPlazasSueltas.label"/> 
					<i class="${alojamientoInstance?.permiteAlquilarPlazasSueltas?'icon-ok':'icon-remove'}" ></i>
				</p>				
				<div class="help-block offset1 text-right">											
					<i class="icon-pencil"></i>
					<font size="-1"><g:message code="alojamiento.permiteAlquilarPlazasSueltas.ayuda" /></font>												
				</div>														
			</div>	
				
		</div>		
											

		<div class="row-fluid">
		
			<div class="span4">
				<hr>
				<h4> <g:message code="alojamiento.caracteristicas"/> </h4>
				<hr>
				<p>
					<g:message code="alojamiento.numeroAseos.label"/> 
					<b><font size="+1"> <g:fieldValue bean="${alojamientoInstance}" field="numeroAseos"/></font></b>
				</p>
				<p>
					<g:message code="alojamiento.tieneAscensor.label"/> 
					<i class="${alojamientoInstance?.tieneAscensor?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneTelevisor.label"/> 
					<i class="${alojamientoInstance?.tieneTelevisor?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneLavadora.label"/> 
					<i class="${alojamientoInstance?.tieneLavadora?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneMicroondas.label"/> 
					<i class="${alojamientoInstance?.tieneMicroondas?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneHorno.label"/> 
					<i class="${alojamientoInstance?.tieneHorno?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneLavavajillas.label"/> 
					<i class="${alojamientoInstance?.tieneLavavajillas?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneSecadora.label"/> 
					<i class="${alojamientoInstance?.tieneSecadora?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneFrigorifico.label"/> 
					<i class="${alojamientoInstance?.tieneFrigorifico?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneCongelador.label"/> 
					<i class="${alojamientoInstance?.tieneCongelador?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneMuebles.label"/> 
					<i class="${alojamientoInstance?.tieneMuebles?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneAireAcondicionado.label"/> 
					<i class="${alojamientoInstance?.tieneAireAcondicionado?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.tieneCalefaccion.label"/> 
					<i class="${alojamientoInstance?.tieneCalefaccion?'icon-ok':'icon-remove'}" ></i>
				</p>
			</div>
			
			<div class="span4">				
				<h4> <g:message code="alojamiento.precioincluye"/> </h4>
				<hr>
				<p>
					<g:message code="alojamiento.precioIncluyeGaraje.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeGaraje?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeTelefono.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeTelefono?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeInternet.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeInternet?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeAgua.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeAgua?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeElectricidad.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeElectricidad?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeComunidad.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeComunidad?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeGas.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeGas?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeLimpieza.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeLimpieza?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeDesayuno.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeDesayuno?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeAlmuerzo.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeAlmuerzo?'icon-ok':'icon-remove'}" ></i>
				</p>
				<p>
					<g:message code="alojamiento.precioIncluyeCena.label"/> 
					<i class="${alojamientoInstance?.precioIncluyeCena?'icon-ok':'icon-remove'}" ></i>
				</p>
			</div>			

			<div class="span4">
				<hr>
				<h4 class="text-center"> <g:message code="alojamiento.observaciones.label"/>: </h4>
				<p class="text-right"> <g:fieldValue bean="${alojamientoInstance}" field="observaciones"/></p>
				<hr>
			</div>	
				
		</div>		
		
				
		<div class="row-fluid">		
			<div class="span11 offset1 text-right">
					<span><g:message code="alojamiento.lastUpdated.label"/>: </span>
					<span><g:formatDate date="${alojamientoInstance?.lastUpdated}" /></span>				
			</div>
		</div>
			
						
									
		<g:if test="${mostrarBotonesEdicion}">			
			<g:form>
				<g:hiddenField name="id" value="${alojamientoInstance?.id}" />
				<div class="form-actions">
					<g:link controller="image" action="add" id="${alojamientoInstance.id}" class="btn btn-danger"><g:message code="list.botonfotos" /> &raquo; </g:link>
					<g:link class="btn" action="edit" id="${alojamientoInstance?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" />
					</g:link>
					
					
				</div>
			</g:form>	
		</g:if>		
	</body>
</html>
