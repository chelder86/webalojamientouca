
<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'alojamiento.label')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	
		<script type="text/javascript">
			$('.linea-horizontal').css({"display": "none"});
			$('#alojamiento').attr('class','active')
		</script>
		
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						
						<g:render template="search"/>
						
					</ul>
				</div>
			</div>

			<div class="span9 text-center">
				
				<br>			

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
								
				<table class="table table-striped">
					<thead>
						<tr>		
						
							<sec:access expression="hasAnyRole('ROL_ADMIN','ROL_SUPERADMIN')">  				
								<th></th>
							</sec:access>
							<th></th>
							
							<g:sortableColumn params="${params}" property="direccion" title="${message(code: 'alojamiento.direccion.label')}" />
						
							<g:sortableColumn params="${params}" property="localidad" title="${message(code: 'alojamiento.localidad.label')}" />
						
							<g:sortableColumn params="${params}" property="precioPorPlazaIndividual" title="${message(code: 'alojamiento.precioPorPlazaIndividual.label')}" />
						
							<g:sortableColumn params="${params}" property="numeroPlazasTotales" title="${message(code: 'alojamiento.numeroPlazasTotales.label')}" />
							
							<g:sortableColumn params="${params}" property="numeroPlazasLibres" title="${message(code: 'alojamiento.numeroPlazasLibres.label')}" />
							
							<g:sortableColumn params="${params}" property="lastUpdated" title="${message(code: 'alojamiento.lastUpdated.label')}" />
						
									
							
						</tr>
					</thead>
					<tbody>
					<g:each in="${alojamientoInstanceList}" var="alojamientoInstance">
						<tr>
	
							<td class="link">
								<g:link action="show" id="${alojamientoInstance.id}" class="btn btn-small btn-inverse"><g:message code="list.botonveralojamiento" /> &raquo; </g:link>
							</td>
							
							
							<sec:access expression="hasAnyRole('ROL_ADMIN','ROL_SUPERADMIN')">   
							
							<td class="link">
								<g:link action="edit" id="${alojamientoInstance.id}" class="btn btn-small"><g:message code="list.botoneditaralojamiento" /> &raquo; </g:link>
							</td>							
								
							</sec:access>
							
							
	
							<td>${fieldValue(bean: alojamientoInstance, field: "direccion")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "localidad.localidad")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "precioPorPlazaIndividual")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "numeroPlazasTotales")}</td>
							
							<td>${fieldValue(bean: alojamientoInstance, field: "numeroPlazasLibres")}</td>
													
							<td><g:formatDate date="${alojamientoInstance.lastUpdated}" /></td>
						
							
						</tr>
					</g:each>
					</tbody>
				</table>
			
			
			<div class="text-right">
			<p class="help-block offset1">
				<i class="icon-pencil"></i>
				<g:message code="list.ayuda" />
			</p>
			</div>		
				
				<div class="pagination">
					<bootstrap:paginate total="${alojamientoInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
