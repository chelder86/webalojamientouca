
<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
<%--		<meta name="layout" content="bootstrap">--%>
<%--		<g:set var="entityName" value="${message(code: 'alojamiento.label')}" />--%>
<%--		<title><g:message code="default.list.label" args="[entityName]" /></title>--%>
	</head>
	<body>
		<div class="row-fluid">			
			<div>
				
				<div>
				<h4> <g:message code="alojamiento.listapublicados" />
				
				 </h4>
				</div>
				
				

				
				<table class="table table-striped">
					<thead>
						<tr>	
						

							<g:sortableColumn params="${params}" property="permiteAlquilarPlazasSueltas" title="${message(code: 'alojamiento.permiteAlquilarPlazasSueltas.label')}" />
						
							<g:sortableColumn params="${params}" property="lastUpdated" title="${message(code: 'alojamiento.lastUpdated.label')}" />
																		
							<g:sortableColumn params="${params}" property="direccion" title="${message(code: 'alojamiento.direccion.label')}" />
						
							<g:sortableColumn params="${params}" property="localidad" title="${message(code: 'alojamiento.localidad.label')}" />
						
							<g:sortableColumn params="${params}" property="precioPorPlazaIndividual" title="${message(code: 'alojamiento.precioPorPlazaIndividual.label')}" />
						
							<g:sortableColumn params="${params}" property="numeroPlazasTotales" title="${message(code: 'alojamiento.numeroPlazasTotales.label')}" />
							
							<g:sortableColumn params="${params}" property="numeroPlazasLibres" title="${message(code: 'alojamiento.numeroPlazasLibres.label')}" />
							

							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${alojamientoInstanceList}" var="alojamientoInstance">
						<tr>

							<td><i class="${alojamientoInstance?.permiteAlquilarPlazasSueltas?'icon-ok':'icon-remove'}" ></i> </td>
												
							<td><g:formatDate date="${alojamientoInstance.lastUpdated}" /></td>
																	
							<td>${fieldValue(bean: alojamientoInstance, field: "direccion")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "localidad.localidad")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "precioPorPlazaIndividual")}</td>
						
							<td>${fieldValue(bean: alojamientoInstance, field: "numeroPlazasTotales")}</td>
							
							<td>${fieldValue(bean: alojamientoInstance, field: "numeroPlazasLibres")}</td>
													

							
							<td class="link">
								<g:link action="show" id="${alojamientoInstance.id}" class="btn btn-small btn-inverse"><g:message code="list.botonveralojamiento" /> &raquo; </g:link>
							</td>
	
							<td class="link">
								<g:link controller="image" action="add" id="${alojamientoInstance.id}" class="btn btn-small btn-danger"><g:message code="list.botonfotos" /> &raquo; </g:link>
							</td>	
							
							<td class="link">
								<g:link action="edit" id="${alojamientoInstance.id}" class="btn btn-small"><g:message code="list.botoneditaralojamiento" /> &raquo; </g:link>
							</td>
							
							
							
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${alojamientoInstanceTotal}" />
				</div>
			</div>

		</div>
		
				
		<%-- Únicamente muestra la lista de los alojamientos del usuario si ya ha publicado alguno: --%>
		<script>
			<g:if test="${alojamientoInstanceTotal == 0}">
				$('#divListUser').hide();                 
			</g:if>                             
		</script>
		
		
	</body>
</html>
