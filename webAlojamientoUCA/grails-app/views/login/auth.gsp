<html>
<head>
<meta name="layout" content="bootstrap" />
<title>Oficina de Alojamiento de la Universidad de Cádiz</title>
</head>

<body>

	<script type="text/javascript">
$('.linea-horizontal').css({"display": "none"});
$('#inicio').attr('class','active');

</script>



	<div class="row-fluid text-right">
		<section id="main" class="span9">
			<div>
				<h4 class="alert alert-block">
					<g:message code="login.bienvenido.titulo" />
				</h4>

				<h5 class="offset2">
					<g:message code="login.bienvenido.descripcion" />
				</h5>
				<br> <br>
			</div>
		</section>
	</div>

	<div class="container text-right">
		<div id='login ' class="span3 offset1 ">
			<div>
				<div>
					<g:message code="login.botonlogin.ayuda" />
				</div>
				<br>

				<form action='${postUrl}' method='POST' id='loginForm' autocomplete='off'>
					<p>
						<label for='username'><g:message
								code="login.username.label" />:</label>
						<input type='text' name='j_username' id='username' />
					</p>

					<p>
						<label for='password'><g:message
								code="login.password.label" />:</label> <input
							type='password' name='j_password' id='password' />
					</p>

					<p id="remember_me_holder">
						<input type='checkbox' name='${rememberMeParameter}' id='remember_me'
							<g:if test='${hasCookie}'>checked='checked'</g:if> /> <label
							for='remember_me'><g:message
								code="login.remember.me.label" /></label>
					</p>

					<p>
						<input class="btn btn-primary" type='submit' id="submit"
							value='${message(code: "login.botonlogin")}' />




					</p>
				</form>
			</div>
		</div>


		<div class="offset1">
			<div class="offset1 span3">

				<g:message code="login.botonregistrarse.ayuda" />

				<div>
					<br>
					<g:link controller="Usuario" action="create">
						<button class="btn btn-success">
							<g:message code="login.botonregistrarse" />
						</button>
					</g:link>
				</div>

			</div>

			<div class="text-left span7">
				<br> <br>

				<g:if test="${flash.message}">
					<bootstrap:alert class="alert-error">
						${flash.message}
					</bootstrap:alert>
				</g:if>
			</div>

		</div>

	</div>


	<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
