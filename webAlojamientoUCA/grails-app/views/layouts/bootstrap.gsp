<%@ page
	import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="${meta(name: 'app.name')}" /></title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

<r:require modules="scaffolding" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir: 'images', file: 'favicon.ico')}"
	type="image/x-icon">
<link rel="apple-touch-icon"
	href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="114x114"
	href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">



<g:layoutHead />

<r:layoutResources />
</head>

<body>

	<div class="row-fluid">

		<div class="container padre-de-posicionar-verticalmente">
			<div class="pull-left">				
					<g:img dir="images" file="logo_oficina_alojamiento.png"
						alt="Logo Oficina Alojamiento" />				
			</div>

			<div class="span1">
				<div class="centrar-verticalmente">
					<div>	
					<g:cambiarIdioma idioma="es"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="zh_CN"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="sv"></g:cambiarIdioma>				
					
					</div>	
					<div>	
					<g:cambiarIdioma idioma="en"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="da"></g:cambiarIdioma>											
					<g:cambiarIdioma idioma="ja"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="fr"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="th"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="it"></g:cambiarIdioma>
					
					<g:cambiarIdioma idioma="nb"></g:cambiarIdioma>	
					</div>
					<div>
					<g:cambiarIdioma idioma="de"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="cs_CZ"></g:cambiarIdioma>	
					
					<g:cambiarIdioma idioma="nl"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="pl"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="ru"></g:cambiarIdioma>
					</div>
					<div>
					<g:cambiarIdioma idioma="pt_BR"></g:cambiarIdioma>
					<g:cambiarIdioma idioma="pt_PT"></g:cambiarIdioma>
					
									
					</div>		
				</div>
			</div>


			<div class="pull-right">
				<a href="http://www.uca.es">
					<g:img dir="images" file="logo_universidad_cadiz.gif"
						alt="Logo Universidad de Cádiz" />
				</a>
			</div>

		</div>

		<div class="linea-horizontal"></div>

		<div class="centrado-redefinido">

			<ul class="nav nav-tabs">

				<li id="usuario"><g:link controller="usuario">
						<g:message code="nav.usuario.show" />
					</g:link></li>

				<li id="contacto"><g:link controller="alojamiento" action="contacto">
						<g:message code="nav.contacto" />
					</g:link></li>

				<li id="inicio"><a href="${createLink(uri: '/')}"> <g:message
							code="nav.home" />
				</a></li>

				<li id="alojamiento"><g:link controller="alojamiento"
						action="list">
						<g:message code="nav.alojamiento" />
					</g:link></li>

				<%-- Si es administrador, puede ver la lista de usuarios:--%>
				<sec:access expression="hasAnyRole('ROL_ADMIN', 'ROL_SUPERADMIN')">
					<li id="listausuarios"><g:link controller="usuario"
							action="list">
							<g:message code="nav.listausuarios" />
						</g:link></li>
				</sec:access>
			</ul>
		</div>

		<sec:ifLoggedIn>
			<div class="pull-right situartransparente-capa-superiora-carousel">
				<p class="logout">
					${sec.loggedInUserInfo(field: 'username')}:
					<g:link controller="logout">
						<button class="btn btn-small btn-info" type="button">
							<g:message code="logout" />
						</button>
					</g:link>
				</p>
			</div>
		</sec:ifLoggedIn>

	</div>



	<div class="container">

		<g:layoutBody />

		<footer>
			<div class="row-fluid span12">
			<%-- <hr>--%>
			<p class="linea-horizontal-footer  "></p>
</div>
			<div class="span6 offset3 text-right footer-texto-creador">
				<a class="logo-esn-cool" href="http://www.erasmuscadiz.com/"> <g:img
						dir="images" file="logo_esn_cool.jpg" alt="Logo ESN cool" />
				</a>
				<p>
					&#62;
					<g:message code="foot.creadordescripcion" />
					Carlos-Helder García Escobar, 2013 &#8594; <a href="#modalCreador"
						role="button" data-toggle="modal"> <g:message
							code="foot.creadorlink" />
					</a>
				</p>
				<p>
					&#62;
					<g:message code="foot.esndescripcion" />
					&#8594; <a href="http://www.erasmuscadiz.com/"> <g:message
							code="foot.esnlink" />
					</a>
				</p>
			</div>

			<div class="pull-right">
				<a href="http://www.erasmuscadiz.com/"> <g:img dir="images"
						file="logo_esn.jpg" alt="Logo ESN" />
				</a>
			</div>
		</footer>
	</div>




	<!-- Twitter Bootstrap Modal: Creadores y Licencia -->
	<div id="modalCreador" class="modal hide fade" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel">
				<g:message code="foot.creadoresylicencia.titulo" />
			</h3>
		</div>
		<div class="modal-body">
			<p>
				<g:message code="foot.creadoresylicencia.creador" />
				<b> Carlos-Helder García Escobar: </b>
			</p>
			<ul>
				<li>email: <a
					href="mailto:helder1986@gmail.com?subject=Web Alojamiento UCA: ">
						helder1986@gmail.com</a>
				</li>
				<li>Linkedin: <a
					href="http://www.linkedin.com/pub/carlos-helder-garc%C3%ADa-escobar/21/b30/224">www.linkedin.com/pub/carlos-helder-garc%C3%ADa-escobar/21/b30/224</a>
				</li>
			</ul>

			<p>
				<g:message code="foot.creadoresylicencia.colaborador" />
				<b> Juan Antonio Caballero Hernández: </b>
			</p>
			<ul>
				<li>email: <a
					href="mailto:juanantonio.caballero@uca.es?subject=Web Alojamiento UCA: ">
						juanantonio.caballero@uca.es</a>
				</li>
			</ul>

			<p>
				<g:message code="foot.creadoresylicencia.licencia" />
				<b> GPLv3. </b>
				<g:message code="foot.creadoresylicencia.licencia.descripcion" />
			</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">
				<g:message code="botoncerrar" />
			</button>
		</div>
	</div>


	<r:layoutResources />

</body>
</html>