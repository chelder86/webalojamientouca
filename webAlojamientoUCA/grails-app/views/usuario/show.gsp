
<%@ page import="usuario.Usuario"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'usuario.label')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>

	<script type="text/javascript">
		$('.linea-horizontal').css({"display": "none"});
		$('#usuario').attr('class','active')
	</script>

	<div class="row-fluid text-center">

		<div class="span11 offset1">

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<dl>

				<g:if test="${usuarioInstance?.username}">
					<dt>
						<g:message code="usuario.username.label"/>
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}" field="username" />
					</dd>

				</g:if>

				<g:if test="${usuarioInstance?.nombre}">
					<dt>
						<g:message code="usuario.nombre.label"/>
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}" field="nombre" />
					</dd>

				</g:if>

				<g:if test="${usuarioInstance?.apellidos}">
					<dt>
						<g:message code="usuario.apellidos.label"/>
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}" field="apellidos" />
					</dd>

				</g:if>

				<g:if test="${usuarioInstance?.email}">
					<dt>
						<g:message code="usuario.email.label"/>
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}" field="email" />
					</dd>

				</g:if>

				<g:if test="${usuarioInstance?.telefono}">
					<dt>
						<g:message code="usuario.telefono.label"/>
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}" field="telefono" />
					</dd>

				</g:if>

				<g:if test="${usuarioInstance?.documentoIdentificacion}">
					<dt>
						<g:message code="usuario.documentoIdentificacion.label" />
					</dt>

					<dd>
						<g:fieldValue bean="${usuarioInstance}"
							field="documentoIdentificacion" />
					</dd>

				</g:if>


				<%-- Sólo los administradores pueden ver estos datos:  --%>
				<sec:access expression="hasAnyRole('ROL_ADMIN', 'ROL_SUPERADMIN')">

					<g:if test="${usuarioInstance?.accountExpired}">
						<dt>
							<g:message code="usuario.accountExpired.label" />
						</dt>

						<dd>
							<g:formatBoolean boolean="${usuarioInstance?.accountExpired}" />
						</dd>

					</g:if>

					<g:if test="${usuarioInstance?.accountLocked}">
						<dt>
							<g:message code="usuario.accountLocked.label" />
						</dt>

						<dd>
							<g:formatBoolean boolean="${usuarioInstance?.accountLocked}" />
						</dd>

					</g:if>

					<g:if test="${usuarioInstance?.enabled}">
						<dt>
							<g:message code="usuario.enabled.label" />
						</dt>

						<dd>
							<g:formatBoolean boolean="${usuarioInstance?.enabled}" />
						</dd>

					</g:if>

					<g:if test="${usuarioInstance?.passwordExpired}">
						<dt>
							<g:message code="usuario.passwordExpired.label" />
						</dt>

						<dd>
							<g:formatBoolean boolean="${usuarioInstance?.passwordExpired}" />
						</dd>
					</g:if>


					<dt>
						<g:message code="usuario.rol.label" />
					</dt>
					<g:each in="${usuarioInstance.getAuthorities()}" var="roles">
						<dd>
							${roles.authority}
						</dd>
					</g:each>
				</sec:access>
			</dl>



			<g:form>
				<g:hiddenField name="id" value="${usuarioInstance?.id}" />
				<div class="form-actions">
					<g:link class="btn btn-info" action="edit" id="${usuarioInstance?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" />
					</g:link>
					<g:link class="btn" action="changePassword" id="${usuarioInstance?.id}">
						<i class="icon-eye-open"></i>
						<g:message code="usuario.botonchangepassword" />
					</g:link>
				</div>
			</g:form>

		</div>

	</div>
</body>
</html>
