<%@ page import="usuario.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'usuario.label')}" />
		<title>${titleText}</title>
	</head>
	<body>
		<div class="row-fluid">
			

			
			<div class="offset2 span8 text-center">

				<div class="page-header">
					<h1> ${titleText} </h1>
				</div>
				
				<dl>
				<dt class="alert alert-error"> ${descriptionText} </dt>
				</dl>

				
								

				
			</div>

		</div>
	</body>
</html>
