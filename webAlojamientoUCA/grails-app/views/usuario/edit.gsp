<%@ page import="usuario.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'usuario.label')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>			
	</head>
	<body>
		<div class="row-fluid">
		
			<div class="span11 offset1">

				<div class="page-header">
					<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${usuarioInstance}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${usuarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>

				<fieldset>
					<g:form class="form-horizontal" action="edit" id="${usuarioInstance?.id}" >
						<g:hiddenField name="version" value="${usuarioInstance?.version}" />
						<fieldset>
						
							<%-- <f:all bean="usuarioInstance"/> --%>							
							<%-- <f:field bean="${usuarioInstance}" property="username" />--%>
							<f:field bean="${usuarioInstance}" property="nombre" />
							<f:field bean="${usuarioInstance}" property="apellidos" />
							<f:field bean="${usuarioInstance}" property="email" />
							<f:field bean="${usuarioInstance}" property="telefono" input-oninvalid="setCustomValidity('${message(code:"usuario.telefono.matches.invalid")}')" />
							<f:field bean="${usuarioInstance}" property="documentoIdentificacion" />	
							<%-- Sólo los administradores pueden ver estos datos:  --%>
							
							<sec:access expression="hasAnyRole('ROL_ADMIN', 'ROL_SUPERADMIN')">						
								<f:field bean="${usuarioInstance}" property="enabled" />
								<f:field bean="${usuarioInstance}" property="accountExpired" />
								<f:field bean="${usuarioInstance}" property="accountLocked" />
								<f:field bean="${usuarioInstance}" property="passwordExpired" />							
							</sec:access>
				

							
							
							<!-- TODO Un superadministrador puede cambiar el rol por defecto (usuario) -->
							<sec:ifAnyGranted roles="ROL_SUPERADMIN">
														
							    <div class="fieldcontain ">
							        <label for="selectorRol">
							            <g:message code="usuario.rol.label"  />
							        </label>
							
							        <div id="formulario_usuario">
							            <g:select
							                optionKey="id" optionValue="authority"
							                name="selectorRol" id="idSelectorRol" from="${usuario.Rol.list()}"
							                value="${rolActualId}"
							            />
							        </div>
							    </div>
							</sec:ifAnyGranted>
							
							
											
							
							<div class="form-actions row-fluid span12">
								<div class="span2 offset1">
									<button type="submit" class="btn btn-primary">
										<i class="icon-ok icon-white"></i>
										<g:message code="default.button.update.label" />
									</button>
								</div>			
								<div class="span2 offset7 ">										
									<button type="submit" class="btn btn-danger" name="_action_delete" onclick="return confirm('${message(code: 'confirmareliminacion')}')" formnovalidate>
										<i class="icon-trash icon-white"></i>
										<g:message code="default.button.delete.label" />
									</button>
								</div>		
							</div>
						</fieldset>
					</g:form>
				</fieldset>

			</div>

		</div>
	</body>
</html>
