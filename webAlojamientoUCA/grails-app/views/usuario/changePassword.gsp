<%@ page import="usuario.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">		
		<title><g:message code="default.changepassword"  /></title>
	</head>
	<body>
		<div class="row-fluid">
		
			<div class="span11 offset1">

				<div class="page-header">
					<h1><g:message code="default.changepassword"/></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${usuarioInstance}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${usuarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>
				
				<%-- Muestra los errores encontrados por el command object si hay alguno: --%>
				<g:hasErrors bean="${changePasswordCommand}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${changePasswordCommand}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>				

				<fieldset>
					<g:form class="form-horizontal" action="changePassword" id="${usuarioInstance?.id}" >
						<g:hiddenField name="version" value="${usuarioInstance?.version}" />
						<fieldset>

							<f:field bean="${usuarioInstance}" property="password" input-type="password" value="" label="usuario.antiguoPassword"/>
							<f:field property="nuevoPassword" label="usuario.nuevoPassword" input-type="password"/>
							<f:field property="confirmarNuevoPassword" label="usuario.confirmarNuevoPassword" input-type="password"/>
							
							
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">
									<i class="icon-ok icon-white"></i>
									<g:message code="default.button.update.label" />
								</button>
							</div>
						</fieldset>
					</g:form>
				</fieldset>

			</div>

		</div>
	</body>
</html>
