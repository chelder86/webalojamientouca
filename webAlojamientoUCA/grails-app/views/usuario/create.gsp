<%@ page import="usuario.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'usuario.label')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
		
			<div class="span11 offset1">

				<div class="page-header">
					<h1><g:message code="default.create.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${usuarioInstance}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${usuarioInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>
				
				
				<%-- Muestra los errores encontrados por el command object si hay alguno: --%>
				<g:hasErrors bean="${checkPasswordCommand}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${checkPasswordCommand}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>
						
								
				<fieldset>
					<g:form class="form-horizontal" action="create" >
						<fieldset>
						
							<%-- <f:all bean="usuarioInstance"/> --%>							
							<f:field bean="${usuarioInstance}" property="username" />
							<f:field bean="${usuarioInstance}" property="nombre" />
							<f:field bean="${usuarioInstance}" property="apellidos" />
							<f:field bean="${usuarioInstance}" property="password" input-type="password"/>
							<f:field property="confirmarPassword" label="usuario.confirmarPassword" input-type="password"/>
							<f:field bean="${usuarioInstance}" property="email" />
							<f:field bean="${usuarioInstance}" property="telefono" input-oninvalid="setCustomValidity('${message(code:"usuario.telefono.matches.invalid")}')" />
							<f:field bean="${usuarioInstance}" property="documentoIdentificacion" />
							<div class="misma-linea-contenedor">
								<div class="misma-linea-A">
								<f:field bean="${usuarioInstance}" property="aceptarPoliticaDeProteccionDeDatos" />
								</div>
								<div class="help-block misma-linea-B">
									<i class="icon-pencil"></i>
									<g:message code="usuario.aceptarPoliticaDeProteccionDeDatos.ayuda" />
								</div>									
							</div>						
																					
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">
									<i class="icon-ok icon-white"></i>
									<g:message code="default.button.create.label" />
								</button>
							</div>
						</fieldset>
					</g:form>
				</fieldset>
				
			</div>

		</div>
	</body>
</html>
