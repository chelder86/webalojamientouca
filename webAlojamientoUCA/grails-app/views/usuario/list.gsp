
<%@ page import="usuario.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'usuario.label')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	
	<script type="text/javascript">
		$('.linea-horizontal').css({"display": "none"});
		$('#listausuarios').attr('class','active');
	</script>	
	
		<div class="row-fluid">
			
			<div class="span11 offset1">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="username" title="${message(code: 'usuario.username.label')}" />
						
							<g:sortableColumn property="nombre" title="${message(code: 'usuario.nombre.label')}" />
						
							<g:sortableColumn property="apellidos" title="${message(code: 'usuario.apellidos.label')}" />
						
							<g:sortableColumn property="email" title="${message(code: 'usuario.email.label')}" />
						
							<g:sortableColumn property="telefono" title="${message(code: 'usuario.telefono.label')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${usuarioInstanceList}" var="usuarioInstance">
						<tr>
						
							<td>${fieldValue(bean: usuarioInstance, field: "username")}</td>
						
							<td>${fieldValue(bean: usuarioInstance, field: "nombre")}</td>
						
							<td>${fieldValue(bean: usuarioInstance, field: "apellidos")}</td>
						
							<td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
						
							<td>${fieldValue(bean: usuarioInstance, field: "telefono")}</td>
						
							<td class="link">
								<g:link action="show" id="${usuarioInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${usuarioInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
