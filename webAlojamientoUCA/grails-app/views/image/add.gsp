<%@ page import="alojamiento.Alojamiento" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">		
		<title><g:message code="image.title" /></title>
		<r:require modules="bootstrap-file-upload"/>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span8 offset1">

				<div class="page-header">
					<h3><g:message code="image.title" /> <span style="color:green;"> ${alojamiento.Alojamiento.get(id).direccion} </span> </h3>
				</div>
				 				 
				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>				 
				 
				<div>
				
				
					<h5>
						<g:message code="image.instrucciones.titulo" />
					</h5>
					<ul>
						<li><g:message code="image.instrucciones.nombredelarchivo" /></li>					
						<li><g:message code="image.instrucciones.tamanomaximo" /></li>
						<li><g:message code="image.instrucciones.formato" /></li>
						<li><g:message code="image.instrucciones.dimensiones" /></li>
					</ul>

					<br>
					<br>
					<br>

					<bsfu:fileUpload action="uploadImage" controller="image" formData="[idAlojamiento:id]" prependFiles="${true}" maxFileSize="${8388608}" />
					<%-- 
					> Parámetro maxNumberOfFiles no funciona correctamente 
					(actualmente, un usuario puede subir un número infinito de fotos)
					bug publicado aquí: https://github.com/sarbogast/grails-bootstrap-file-upload/issues/14	
					--%>
		
		
				</div>



			</div>

		</div>
	</body>
</html>
