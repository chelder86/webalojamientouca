<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap" />
<title>Oficina de Alojamiento de la Universidad de Cádiz</title>
</head>

<body>

	<script type="text/javascript">
		$('.linea-horizontal').css({"display": "none"});
		$('#inicio').attr('class','active');
	</script>


	<div class="row-fluid span12 text-right">
		<section id="main" class="span9">
			<div>
				<h4 class="alert alert-info">
					<g:message code="index.bienvenido.titulo" />
				</h4>
				<p class="offset2">
					<g:message code="index.bienvenido.descripcion" />
				</p>
			</div>
		<br>
		<br>			
		</section>		

	</div>




	<div class="row-fluid span12">

		<sec:access expression="isAuthenticated()">
			<div class="span3 offset1 text-right">
				<g:link controller="alojamiento" action="create">
					<button class="btn btn-large btn-success">
						<g:message code="index.botonofertar" />
					</button>
				</g:link>
				<p>
					<g:message code="index.botonofertar.ayuda" />
				</p>
			</div>
		</sec:access>

		<sec:access expression="hasRole('ROL_USUARIO')">
			<div class="span3 offset1">

				<g:link controller="Alojamiento" action="list">
					<button class="btn btn-large btn-primary">
						<g:message code="index.botonbuscaralojamiento" />
					</button>
				</g:link>
				<p>
					<g:message code="index.botonbuscaralojamiento.ayuda" />
				</p>
			</div>
		</sec:access>

		<%-- Si es un administrador, además de buscar un alojamiento puede editar cualquiera y aquí se le avisa con un texto: --%>
		<%-- Además, puede ver la lista de usuarios --%>
		<sec:access expression="hasAnyRole('ROL_ADMIN', 'ROL_SUPERADMIN')">
			<div class="span3 offset1">

				<g:link controller="Alojamiento" action="list">
					<button class="btn btn-large btn-danger">
						<g:message code="index.botonbuscaryeditaralojamiento" />
					</button>
				</g:link>
				<p>
					<g:message code="index.botonbuscaryeditaralojamiento.ayuda" />
				</p>
			</div>


			<div class="span3 offset1">

				<g:link controller="Usuario" action="list">
					<button class="btn btn-large btn-warning">
						<g:message code="index.botonlistausuarios" />
					</button>
				</g:link>
				<p>
					<g:message code="index.botonlistausuarios.ayuda" />
				</p>
			</div>

		</sec:access>

	</div>




	<sec:access expression="isAuthenticated()">

		<div class="row-fluid span12">
			<h4>
				<g:if test="${flash.message}">
					<bootstrap:alert class="alert-error">
						${flash.message}
					</bootstrap:alert>
				</g:if>
			</h4>
		</div>


		<div id="divListUser" class="row-fluid span12 text-left">
			<div>
				<g:include controller="alojamiento" action="userList" />
			</div>
		</div>


	</sec:access>


	<%-- El superadministrador puede ver todas las funciones extras: --%>
	<sec:access expression="hasRole('ROL_SUPERADMIN')">

		<div class="row-fluid ">



			<h4>
				Only the superadministrators can see the following section:
			</h4>

			<div class="span3">
				<br>
				<g:link controller="Usuario" action="allEmails">
					<button class="btn btn-large btn-inverse">
						List all emails
					</button>
				</g:link>
			</div>

			<div class="span3">
				<h5>Application Status</h5>
				<ul>
					<li>App version: <g:meta name="app.version" /></li>
					<li>Grails version: <g:meta name="app.grails.version" /></li>
					<li>Groovy version: ${groovy.lang.GroovySystem.getVersion()}</li>
					<li>JVM version: ${System.getProperty('java.version')}</li>
					<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
					<li>Domains: ${grailsApplication.domainClasses.size()}</li>
					<li>Services: ${grailsApplication.serviceClasses.size()}</li>
					<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
				</ul>
			</div>
			<div class="span3">
				<h5>Installed Plugins</h5>
				<ul>
					<g:each var="plugin"
						in="${applicationContext.getBean('pluginManager').allPlugins}">
						<li>
							${plugin.name} - ${plugin.version}
						</li>
					</g:each>
				</ul>
			</div>
		</div>


	</sec:access>



</body>
</html>
