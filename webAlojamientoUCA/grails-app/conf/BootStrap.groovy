import alojamiento.Localidad
import usuario.*

class BootStrap {
	
	transient springSecurityService	
	def fixtureLoader

    def init = { servletContext ->
		
		// Creación de todas las localidades de la provincia de Cádiz:
		List<String> localidadesCadiz  = ["Cádiz: centro", "Cádiz: parte nueva", "Jerez de la Frontera", "Alcalá de los Gazules", "Alcalá del Valle", "Algar", "Algeciras", "Algodonales", "Arcos de la Frontera", "Barbate", "Los Barrios", "Benalup-Casas Viejas", "Benaocaz", "Bornos", "El Bosque", "Castellar de la Frontera", "Chiclana de la Frontera", "Chipiona", "Conil de la Frontera", "Espera", "El Gastor", "Grazalema", "Jimena de la Frontera", "La Línea de la Concepción", "Medina-Sidonia", "Olvera", "Paterna de Rivera", "Prado del Rey", "El Puerto de Santa María", "Puerto Real", "Puerto Serrano", "Rota", "San Fernando", "San José del Valle", "San Roque", "Sanlúcar de Barrameda", "Setenil de las Bodegas", "Tarifa", "Torre Alháquime", "Trebujena", "Ubrique", "Vejer de la Frontera", "Villaluenga del Rosario", "Villamartín", "Zahara de la Sierra"];
		for (String localidadCadiz : localidadesCadiz) {
			def localidadVariable = Localidad.findByLocalidad(localidadCadiz) ?: new Localidad(localidad: localidadCadiz).save(failOnError: true)
		};
	
		// Creación de los roles por defecto
		def userRole = Rol.findByAuthority('ROL_USUARIO') ?: new Rol(authority: 'ROL_USUARIO').save(failOnError: true)
		def adminRole = Rol.findByAuthority('ROL_ADMIN') ?: new Rol(authority: 'ROL_ADMIN').save(failOnError: true)
		def superadminRole = Rol.findByAuthority('ROL_SUPERADMIN') ?: new Rol(authority: 'ROL_SUPERADMIN').save(failOnError: true)

		// XXX testing: si ya existe un superadministrador, este no se creará
		// Si no existe algún superadministrador crea uno por defecto
		if (UsuarioRol.findByRol(superadminRole) == null) {
			def superadminUser = new Usuario( 
					username: 'superadmin',
					password: 'superadmin', confirmarPassword: 'superadmin',
					nombre: 'cambiar password', apellidos: 'cambiar password', 
					email: 'alojamiento@uca.es', 
					telefono: '+0 (00) 00000', documentoIdentificacion: '0000000',
					aceptarPoliticaDeProteccionDeDatos: true).save(failOnError: true)
			
			UsuarioRol.create superadminUser, superadminRole				
		}


		// Si no estamos creando un .war para producción, generamos datos ejemplo para facilitar las pruebas:
		if (!(grails.util.Environment.getCurrent().getName().equals("production"))) {
			fixtureLoader.load("datosEjemploDePrueba")
		}

	}
	def destroy = {
	}
}
