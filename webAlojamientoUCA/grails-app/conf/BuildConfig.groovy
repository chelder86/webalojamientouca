grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.

        // runtime 'mysql:mysql-connector-java:5.1.22'
		
		// Usada principalmente en UploadImageService; http://code.google.com/p/thumbnailator/wiki/Examples
		compile "net.coobird:thumbnailator:0.4.5"
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.2"

        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"

        build ":tomcat:$grailsVersion"

        runtime ":database-migration:1.3.2"

        compile ':cache:1.0.1'
		
		/*
		 * A continuación los plugins que se han instalado
		 * aparte de los que vienen por defecto
		 */
		
		// La interfaz de usuario usa este plugin: grails.org/plugin/twitter-bootstrap:
		runtime ':twitter-bootstrap:2.3.2'
		
		// Fields plugin:
		compile ":fields:1.3"
		
		// Login, logout, control de acceso..: Spring Security Core plugin:
		compile ":spring-security-core:1.2.7.3"
		
		// Para enviar emails:
		//compile ":mail:1.0.1" //> De momento no se está usando. En Config.groovy se incluye, comentada, una configuración que se comprobó que funcionaba correctamente.
		
		// Interfaz chula para subir las fotos de los alojamientos 
		compile (":bootstrap-file-upload:2.1.2") {			
			transitive = false //evita la instalación de bibliotecas antiguas además de las ya más nuevas instaladas (=excludes 'jquery', 'twitter-bootstrap')
		}
		
		// JQuery UI plugin:
		compile ":jquery-ui:1.8.24"
		
		// Carga datos de prueba en la base de datos 
		compile ":fixtures:1.2"
		// compile ":build-test-data:2.0.5" //> Puede que sea necesario; ver: http://grails.org/plugin/fixtures
		
		// Internacionaliza javascript: grails.org/plugin/jawr
		compile ":jawr:3.3.3"
    }
}
