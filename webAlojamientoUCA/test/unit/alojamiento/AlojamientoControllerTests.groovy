package alojamiento



import org.junit.*
import grails.test.mixin.*

@TestFor(AlojamientoController)
@Mock(Alojamiento)
class AlojamientoControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/alojamiento/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.alojamientoInstanceList.size() == 0
        assert model.alojamientoInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.alojamientoInstance != null
    }

    void testSave() {
        controller.save()

        assert model.alojamientoInstance != null
        assert view == '/alojamiento/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/alojamiento/show/1'
        assert controller.flash.message != null
        assert Alojamiento.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/alojamiento/list'

        populateValidParams(params)
        def alojamiento = new Alojamiento(params)

        assert alojamiento.save() != null

        params.id = alojamiento.id

        def model = controller.show()

        assert model.alojamientoInstance == alojamiento
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/alojamiento/list'

        populateValidParams(params)
        def alojamiento = new Alojamiento(params)

        assert alojamiento.save() != null

        params.id = alojamiento.id

        def model = controller.edit()

        assert model.alojamientoInstance == alojamiento
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/alojamiento/list'

        response.reset()

        populateValidParams(params)
        def alojamiento = new Alojamiento(params)

        assert alojamiento.save() != null

        // test invalid parameters in update
        params.id = alojamiento.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/alojamiento/edit"
        assert model.alojamientoInstance != null

        alojamiento.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/alojamiento/show/$alojamiento.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        alojamiento.clearErrors()

        populateValidParams(params)
        params.id = alojamiento.id
        params.version = -1
        controller.update()

        assert view == "/alojamiento/edit"
        assert model.alojamientoInstance != null
        assert model.alojamientoInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/alojamiento/list'

        response.reset()

        populateValidParams(params)
        def alojamiento = new Alojamiento(params)

        assert alojamiento.save() != null
        assert Alojamiento.count() == 1

        params.id = alojamiento.id

        controller.delete()

        assert Alojamiento.count() == 0
        assert Alojamiento.get(alojamiento.id) == null
        assert response.redirectedUrl == '/alojamiento/list'
    }
}
