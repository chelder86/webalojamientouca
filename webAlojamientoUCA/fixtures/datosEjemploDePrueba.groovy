//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.Calendar;

import usuario.*
import alojamiento.*

def userRole = Rol.findByAuthority('ROL_USUARIO')
def adminRole = Rol.findByAuthority('ROL_ADMIN')
//def superAdminRole = Rol.findByAuthority('ROL_SUPERADMIN') //> Ya se crea en BootStrap.groovy si no existiese ninguno

// Tomamos 3 localidades
def cadizcentro = Localidad.findByLocalidad('Cádiz: centro')
def cadiznueva = Localidad.findByLocalidad('Cádiz: parte nueva')
def rota = Localidad.findByLocalidad('Rota')

/* No es necesario puesto que se crea-modifica automáticamente
// Creamos fechas
Calendar miFecha = new GregorianCalendar(2010, Calendar.MARCH, 15);
Date dateModificacionP1 = miFecha.getTime();
miFecha.set(2010, Calendar.MARCH, 30)
*/


fixture{	

	// Creamos 3 usuarios de prueba cada uno con un rol distinto:
	usuario(
		Usuario, username: 'usuario', 
		password: 'usuario', /*confirmarPassword: 'usuario',*/
		nombre: 'Juan Antonio',	apellidos: 'Caballero',	email: 'juanan@gmail.com',	telefono: '666777888',	
		documentoIdentificacion: '12345678C', 		
		aceptarPoliticaDeProteccionDeDatos: true
	)
	admin(
		Usuario, username: 'admin', password: 'admin', /*confirmarPassword: 'usuario',*/
		nombre: 'Carlos-Helder',
		apellidos: 'Garcia', email: 'helder1986@gmail.com',	telefono: '655888999',
		documentoIdentificacion: '58745678P', 
		enabled: true, 
		aceptarPoliticaDeProteccionDeDatos: true
	)
	
	
	// Asignamos los respectivos roles a cada usuario:
	usuarioRol(UsuarioRol, usuario: usuario, rol: userRole)
	adminRol(UsuarioRol, usuario: admin, rol: adminRole)
		
	
	// Creamos 3 alojamientos de prueba; 1 al usuario, 2 al admin (y 0 al superadmin):
	alojamiento1(Alojamiento, 
		localidad: rota, 
		direccion: 'c/charco, 14', 
		latitud_norte: 37.89176,
		longitud_oeste: -6.558082,
		precioPorPlazaIndividual: 150,
		numeroPlazasTotales: 5, 
		numeroPlazasLibres: 4,		
		permiteAlquilarPlazasSueltas: true, 
		datosContacto: 'El casero vive en el molino, su numero es: 655778899', 
		observaciones: 'I speak English',
		usuario: usuario, 
		//lastUpdated: dateModificacionP1, //> No es necesario puesto que se crea-modifica automáticamente
		numeroAseos: 2,		
		tieneAscensor: true, tieneTelevisor: true, esAptoParaMinusvalidos: true, precioIncluyeAgua: true
	)
		
	
	alojamiento2(Alojamiento, 
		localidad: cadiznueva, 
		direccion: 'c/Cabezo, 26', 
		latitud_norte: 36.618921,
		longitud_oeste: -6.362135,
		precioPorPlazaIndividual: 200,
		numeroPlazasTotales: 3, 
		numeroPlazasLibres: 3,		
		permiteAlquilarPlazasSueltas: true, 
		datosContacto: 'De 3 a 7, por favor: 655778899', 
		//observaciones: '', //> No es obligatorio escribir algo en este campo; en este caso lo dejamos vacío.
		usuario: admin, 
		//lastUpdated: dateModificacionP1, //> No es necesario puesto que se crea-modifica automáticamente
		numeroAseos: 3,		
		permiteFumadores: true, precioIncluyeComunidad: true, tieneHorno: true, precioIncluyeAgua: true
	)
	
	alojamiento3(Alojamiento, 
		localidad: cadizcentro, 
		direccion: 'c/charco, 14', 
		latitud_norte: 36.618921,
		longitud_oeste: -6.362135,
		precioPorPlazaIndividual: 200,
		numeroPlazasTotales: 3, 
		numeroPlazasLibres: 0,		
		permiteAlquilarPlazasSueltas: false, 
		datosContacto: 'mejor llamar por la mañana: 655778899, email: asdfasd@asdfasd.com', 
		observaciones: 'A dos minutos de la facultad de ciencias',
		usuario: admin, 
		//lastUpdated: dateModificacionP1, //> No es necesario puesto que se crea-modifica automáticamente
		numeroAseos: 2,		
		tieneMuebles: true, tieneAscensor: true, tieneLavadora: true, precioIncluyeAgua: true		
	)
	
	
	
}